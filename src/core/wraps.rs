
pub mod ash { pub mod vk {

	use std::cmp::{Ordering, PartialOrd};
	use ash::vk;

	pub struct PhysicalDeviceFeatures (vk::PhysicalDeviceFeatures);
	
	impl PhysicalDeviceFeatures {

		pub fn le( a : & vk::PhysicalDeviceFeatures, b : & vk::PhysicalDeviceFeatures) -> bool { 
			let comp = PhysicalDeviceFeatures::partial_cmp(a, b);
			comp == Some(Ordering::Less) || comp == Some(Ordering::Equal)
		}

		/* from : https://docs.rs/ash/latest/ash/vk/struct.PhysicalDeviceFeatures.html
		 * Array.from(document.querySelectorAll(".structfield.small-section-header")).map(x => x.innerText.split(":")[0]).reduce((x, y) => x+`if a.${y} != 0 && b.${y} == 0 {a_has += 1};\nif a.${y} == 0 && b.${y} != 0 {b_has += 1};\n`, "" )
		 * TODO: generated, etc (same as eq()) TODO use macro_rules!, check codeblock variable interpolation
		 */
		pub fn partial_cmp( a : & vk::PhysicalDeviceFeatures, b : & vk::PhysicalDeviceFeatures) -> Option<Ordering> {
			let mut a_has = 0u8;
			let mut b_has = 0u8;

			if a.robust_buffer_access != 0 && b.robust_buffer_access == 0 {a_has += 1};
			if a.robust_buffer_access == 0 && b.robust_buffer_access != 0 {b_has += 1};
			if a.full_draw_index_uint32 != 0 && b.full_draw_index_uint32 == 0 {a_has += 1};
			if a.full_draw_index_uint32 == 0 && b.full_draw_index_uint32 != 0 {b_has += 1};
			if a.image_cube_array != 0 && b.image_cube_array == 0 {a_has += 1};
			if a.image_cube_array == 0 && b.image_cube_array != 0 {b_has += 1};
			if a.independent_blend != 0 && b.independent_blend == 0 {a_has += 1};
			if a.independent_blend == 0 && b.independent_blend != 0 {b_has += 1};
			if a.geometry_shader != 0 && b.geometry_shader == 0 {a_has += 1};
			if a.geometry_shader == 0 && b.geometry_shader != 0 {b_has += 1};
			if a.tessellation_shader != 0 && b.tessellation_shader == 0 {a_has += 1};
			if a.tessellation_shader == 0 && b.tessellation_shader != 0 {b_has += 1};
			if a.sample_rate_shading != 0 && b.sample_rate_shading == 0 {a_has += 1};
			if a.sample_rate_shading == 0 && b.sample_rate_shading != 0 {b_has += 1};
			if a.dual_src_blend != 0 && b.dual_src_blend == 0 {a_has += 1};
			if a.dual_src_blend == 0 && b.dual_src_blend != 0 {b_has += 1};
			if a.logic_op != 0 && b.logic_op == 0 {a_has += 1};
			if a.logic_op == 0 && b.logic_op != 0 {b_has += 1};
			if a.multi_draw_indirect != 0 && b.multi_draw_indirect == 0 {a_has += 1};
			if a.multi_draw_indirect == 0 && b.multi_draw_indirect != 0 {b_has += 1};
			if a.draw_indirect_first_instance != 0 && b.draw_indirect_first_instance == 0 {a_has += 1};
			if a.draw_indirect_first_instance == 0 && b.draw_indirect_first_instance != 0 {b_has += 1};
			if a.depth_clamp != 0 && b.depth_clamp == 0 {a_has += 1};
			if a.depth_clamp == 0 && b.depth_clamp != 0 {b_has += 1};
			if a.depth_bias_clamp != 0 && b.depth_bias_clamp == 0 {a_has += 1};
			if a.depth_bias_clamp == 0 && b.depth_bias_clamp != 0 {b_has += 1};
			if a.fill_mode_non_solid != 0 && b.fill_mode_non_solid == 0 {a_has += 1};
			if a.fill_mode_non_solid == 0 && b.fill_mode_non_solid != 0 {b_has += 1};
			if a.depth_bounds != 0 && b.depth_bounds == 0 {a_has += 1};
			if a.depth_bounds == 0 && b.depth_bounds != 0 {b_has += 1};
			if a.wide_lines != 0 && b.wide_lines == 0 {a_has += 1};
			if a.wide_lines == 0 && b.wide_lines != 0 {b_has += 1};
			if a.large_points != 0 && b.large_points == 0 {a_has += 1};
			if a.large_points == 0 && b.large_points != 0 {b_has += 1};
			if a.alpha_to_one != 0 && b.alpha_to_one == 0 {a_has += 1};
			if a.alpha_to_one == 0 && b.alpha_to_one != 0 {b_has += 1};
			if a.multi_viewport != 0 && b.multi_viewport == 0 {a_has += 1};
			if a.multi_viewport == 0 && b.multi_viewport != 0 {b_has += 1};
			if a.sampler_anisotropy != 0 && b.sampler_anisotropy == 0 {a_has += 1};
			if a.sampler_anisotropy == 0 && b.sampler_anisotropy != 0 {b_has += 1};
			if a.texture_compression_etc2 != 0 && b.texture_compression_etc2 == 0 {a_has += 1};
			if a.texture_compression_etc2 == 0 && b.texture_compression_etc2 != 0 {b_has += 1};
			if a.texture_compression_astc_ldr != 0 && b.texture_compression_astc_ldr == 0 {a_has += 1};
			if a.texture_compression_astc_ldr == 0 && b.texture_compression_astc_ldr != 0 {b_has += 1};
			if a.texture_compression_bc != 0 && b.texture_compression_bc == 0 {a_has += 1};
			if a.texture_compression_bc == 0 && b.texture_compression_bc != 0 {b_has += 1};
			if a.occlusion_query_precise != 0 && b.occlusion_query_precise == 0 {a_has += 1};
			if a.occlusion_query_precise == 0 && b.occlusion_query_precise != 0 {b_has += 1};
			if a.pipeline_statistics_query != 0 && b.pipeline_statistics_query == 0 {a_has += 1};
			if a.pipeline_statistics_query == 0 && b.pipeline_statistics_query != 0 {b_has += 1};
			if a.vertex_pipeline_stores_and_atomics != 0 && b.vertex_pipeline_stores_and_atomics == 0 {a_has += 1};
			if a.vertex_pipeline_stores_and_atomics == 0 && b.vertex_pipeline_stores_and_atomics != 0 {b_has += 1};
			if a.fragment_stores_and_atomics != 0 && b.fragment_stores_and_atomics == 0 {a_has += 1};
			if a.fragment_stores_and_atomics == 0 && b.fragment_stores_and_atomics != 0 {b_has += 1};
			if a.shader_tessellation_and_geometry_point_size != 0 && b.shader_tessellation_and_geometry_point_size == 0 {a_has += 1};
			if a.shader_tessellation_and_geometry_point_size == 0 && b.shader_tessellation_and_geometry_point_size != 0 {b_has += 1};
			if a.shader_image_gather_extended != 0 && b.shader_image_gather_extended == 0 {a_has += 1};
			if a.shader_image_gather_extended == 0 && b.shader_image_gather_extended != 0 {b_has += 1};
			if a.shader_storage_image_extended_formats != 0 && b.shader_storage_image_extended_formats == 0 {a_has += 1};
			if a.shader_storage_image_extended_formats == 0 && b.shader_storage_image_extended_formats != 0 {b_has += 1};
			if a.shader_storage_image_multisample != 0 && b.shader_storage_image_multisample == 0 {a_has += 1};
			if a.shader_storage_image_multisample == 0 && b.shader_storage_image_multisample != 0 {b_has += 1};
			if a.shader_storage_image_read_without_format != 0 && b.shader_storage_image_read_without_format == 0 {a_has += 1};
			if a.shader_storage_image_read_without_format == 0 && b.shader_storage_image_read_without_format != 0 {b_has += 1};
			if a.shader_storage_image_write_without_format != 0 && b.shader_storage_image_write_without_format == 0 {a_has += 1};
			if a.shader_storage_image_write_without_format == 0 && b.shader_storage_image_write_without_format != 0 {b_has += 1};
			if a.shader_uniform_buffer_array_dynamic_indexing != 0 && b.shader_uniform_buffer_array_dynamic_indexing == 0 {a_has += 1};
			if a.shader_uniform_buffer_array_dynamic_indexing == 0 && b.shader_uniform_buffer_array_dynamic_indexing != 0 {b_has += 1};
			if a.shader_sampled_image_array_dynamic_indexing != 0 && b.shader_sampled_image_array_dynamic_indexing == 0 {a_has += 1};
			if a.shader_sampled_image_array_dynamic_indexing == 0 && b.shader_sampled_image_array_dynamic_indexing != 0 {b_has += 1};
			if a.shader_storage_buffer_array_dynamic_indexing != 0 && b.shader_storage_buffer_array_dynamic_indexing == 0 {a_has += 1};
			if a.shader_storage_buffer_array_dynamic_indexing == 0 && b.shader_storage_buffer_array_dynamic_indexing != 0 {b_has += 1};
			if a.shader_storage_image_array_dynamic_indexing != 0 && b.shader_storage_image_array_dynamic_indexing == 0 {a_has += 1};
			if a.shader_storage_image_array_dynamic_indexing == 0 && b.shader_storage_image_array_dynamic_indexing != 0 {b_has += 1};
			if a.shader_clip_distance != 0 && b.shader_clip_distance == 0 {a_has += 1};
			if a.shader_clip_distance == 0 && b.shader_clip_distance != 0 {b_has += 1};
			if a.shader_cull_distance != 0 && b.shader_cull_distance == 0 {a_has += 1};
			if a.shader_cull_distance == 0 && b.shader_cull_distance != 0 {b_has += 1};
			if a.shader_float64 != 0 && b.shader_float64 == 0 {a_has += 1};
			if a.shader_float64 == 0 && b.shader_float64 != 0 {b_has += 1};
			if a.shader_int64 != 0 && b.shader_int64 == 0 {a_has += 1};
			if a.shader_int64 == 0 && b.shader_int64 != 0 {b_has += 1};
			if a.shader_int16 != 0 && b.shader_int16 == 0 {a_has += 1};
			if a.shader_int16 == 0 && b.shader_int16 != 0 {b_has += 1};
			if a.shader_resource_residency != 0 && b.shader_resource_residency == 0 {a_has += 1};
			if a.shader_resource_residency == 0 && b.shader_resource_residency != 0 {b_has += 1};
			if a.shader_resource_min_lod != 0 && b.shader_resource_min_lod == 0 {a_has += 1};
			if a.shader_resource_min_lod == 0 && b.shader_resource_min_lod != 0 {b_has += 1};
			if a.sparse_binding != 0 && b.sparse_binding == 0 {a_has += 1};
			if a.sparse_binding == 0 && b.sparse_binding != 0 {b_has += 1};
			if a.sparse_residency_buffer != 0 && b.sparse_residency_buffer == 0 {a_has += 1};
			if a.sparse_residency_buffer == 0 && b.sparse_residency_buffer != 0 {b_has += 1};
			if a.sparse_residency_image2_d != 0 && b.sparse_residency_image2_d == 0 {a_has += 1};
			if a.sparse_residency_image2_d == 0 && b.sparse_residency_image2_d != 0 {b_has += 1};
			if a.sparse_residency_image3_d != 0 && b.sparse_residency_image3_d == 0 {a_has += 1};
			if a.sparse_residency_image3_d == 0 && b.sparse_residency_image3_d != 0 {b_has += 1};
			if a.sparse_residency2_samples != 0 && b.sparse_residency2_samples == 0 {a_has += 1};
			if a.sparse_residency2_samples == 0 && b.sparse_residency2_samples != 0 {b_has += 1};
			if a.sparse_residency4_samples != 0 && b.sparse_residency4_samples == 0 {a_has += 1};
			if a.sparse_residency4_samples == 0 && b.sparse_residency4_samples != 0 {b_has += 1};
			if a.sparse_residency8_samples != 0 && b.sparse_residency8_samples == 0 {a_has += 1};
			if a.sparse_residency8_samples == 0 && b.sparse_residency8_samples != 0 {b_has += 1};
			if a.sparse_residency16_samples != 0 && b.sparse_residency16_samples == 0 {a_has += 1};
			if a.sparse_residency16_samples == 0 && b.sparse_residency16_samples != 0 {b_has += 1};
			if a.sparse_residency_aliased != 0 && b.sparse_residency_aliased == 0 {a_has += 1};
			if a.sparse_residency_aliased == 0 && b.sparse_residency_aliased != 0 {b_has += 1};
			if a.variable_multisample_rate != 0 && b.variable_multisample_rate == 0 {a_has += 1};
			if a.variable_multisample_rate == 0 && b.variable_multisample_rate != 0 {b_has += 1};
			if a.inherited_queries != 0 && b.inherited_queries == 0 {a_has += 1};
			if a.inherited_queries == 0 && b.inherited_queries != 0 {b_has += 1};

			return match (a_has, b_has) {
				(0, 0) => Some(Ordering::Equal),
				(0, ..) => Some(Ordering::Less),
				(.., 0) => Some(Ordering::Greater),
				_ => None
			}
		}

	}

	impl PartialEq for PhysicalDeviceFeatures {
		fn eq( &self, other: &Self) -> bool {
			PhysicalDeviceFeatures::partial_cmp(&self.0, &other.0) == Some(Ordering::Equal)
		}
	}

	impl PartialOrd for PhysicalDeviceFeatures {
		fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
			PhysicalDeviceFeatures::partial_cmp(&self.0, &other.0)
		}
	}
} }
