
use std::{fs::{File, self}, ops::Add, mem::size_of, sync::{Arc, RwLock}, cell::RefCell, time::{SystemTime, UNIX_EPOCH}, f64::consts::PI, marker::PhantomData, os::raw::c_void};

use ash::{vk, util::read_spv}; use winit::{event_loop::{EventLoop, ControlFlow}, platform::run_return::EventLoopExtRunReturn, event::{WindowEvent, ElementState, VirtualKeyCode, Event, KeyboardInput}};

use crate::core::{os::OSWindow, compute::{vulkan::{APIFeatures, APIInstance, WindowSurface, Swapchain, DeviceFeatures, resources::{DeviceImage, RenderType, DeviceBuffer}, RenderScene, RenderComponent, Renderer}, resources::{Image, Format, buffers::{VertexBase3D, HostBufferPtr, VertexBuffer, IndexBuffer}, scene::{Material, Mesh}}}, math::geometry::polytopes::regular::ncube_nfaces};

use super::compute::{vulkan::{ComputeDevice, ComputeResult, resources::DeviceShaderModule}, resources::scene::ShaderModule};

pub unsafe fn load_shader<'a>(device: &'a ComputeDevice<'a>, path: &String) 
-> ComputeResult<ShaderModule<DeviceShaderModule<'a>>> {
    let mut file = File::open(path).unwrap();
    let code = read_spv(&mut file).unwrap();

    let module = device.create_shader_module(code.as_slice())?;

    Ok(ShaderModule {
	path: path.clone(),
	code: String::from(""),
	module: Some(DeviceShaderModule {
	    device,
	    module,
	}),
    })
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct SpatialComponent3D {
    pub pos: [f32;4],
    pub rot: [f32;16],
}

use glam::{Mat4, Quat, Vec3};

#[derive(Debug)]
pub struct WorldUBO {
    pub model: Mat4,
    pub view: Mat4,
    pub proj: Mat4,
}

impl WorldUBO {
    // pub fn update(&mut self, delta_time: f32) {
	// let axis = Vec3::Z;
	// let rotation_speed = 0.5;
	// let rotation_angle = delta_time * rotation_speed;
	// let rotation_quat = Quat::from_axis_angle(axis, rotation_angle);
	// self.model = Mat4::from_quat(rotation_quat) * self.model;
    // }
    pub fn update(&mut self, start_time: SystemTime, aspect_ratio: f32) {
	let start_time = start_time.duration_since(UNIX_EPOCH).expect("Time went backwards");

	let current_time = SystemTime::now();
	let current_time = current_time.duration_since(UNIX_EPOCH).expect("Time went backwards");
	let period = 5 * 1000;
	// let current_hour_millis = ((current_time.as_secs() / 3600) as u128) * 3600 * 1000;
	// let delta = (((current_time.as_millis() - current_hour_millis) % period) as f64) / period as f64;
	//
	let delta = (((current_time.as_millis() - start_time.as_millis()) % period) as f64) / period as f64;
	let rotation_angle = 2f64*PI*delta;

	let axis = Vec3::Z;
	// let rotation_speed = 0.5;
	// let rotation_angle = 0.1 * rotation_speed;
	// let rotation_quat = Quat::from_axis_angle(axis, (dt as f32)*rotation_angle);
	let rotation_quat = Quat::from_axis_angle(axis, rotation_angle as f32);
	// self.model = Mat4::from_quat(rotation_quat) * self.model;

	self.model = Mat4::from_quat(rotation_quat) * Mat4::IDENTITY;

	let mut coordinate_transform = Mat4::IDENTITY.clone();
	coordinate_transform.z_axis[2] = -1f32;

	let view = Mat4::look_at_rh(Vec3::new(0.0, -0.2, -4.0), Vec3::new(0.0, 0.0, 0.0), Vec3::new(0.0, 0.0, 1.0));

	self.view = view * coordinate_transform;
	self.proj = Mat4::perspective_rh(90.0f32.to_radians(), aspect_ratio, 0.1, 100.0);

	// self.proj.y_axis[1] *= -1f32;

	// println!("ubo update: aspect_ratio: {:?} ", aspect_ratio);
	// println!("model: {:#?} ", self.model);
	// println!("view: {:#?} ", self.view);
	// println!("proj: {:#?} ", self.proj);
    }
}

impl Default for WorldUBO {
    fn default() -> Self {
	Self { model: Mat4::IDENTITY, view: Mat4::IDENTITY, proj: Mat4::IDENTITY }
    }
}

pub fn generic() {
    println!("Simulation wrapper");

	/////////////// window surface

	/////////////// world data, resources

    // ComputeContext::new();
	//
	///////////// build window
	let shaders_path = String::from("resources/shaders");
	let vert_path = shaders_path.clone().add("/vert.spv");
	let frag_path = shaders_path.clone().add("/frag.spv");

	let entries = fs::read_dir("./").unwrap();

	let window_width = 1024u32;
	let window_height = 768u32;

	let mut event_loop = EventLoop::new();
	let event_loop_wrapp = Arc::new(RwLock::new(event_loop));

	let window = OSWindow::new(window_width, window_height, 
				   &String::from("Vulkan Triangle Test"), &event_loop_wrapp);

	let api_features = APIFeatures {
			param_window_builder: Option::Some(&window.os_window),
			..Default::default()
		};
	let api_instance = APIInstance::new(&String::from("NEngine Test"), 
					    &String::from("NEngine"), 
					    &api_features);
	println!("created APIInstance!!");

	let surface = WindowSurface::new(&api_instance, &window);
	println!("created WindowSurface!!");

	// let device_features = DeviceFeatures {
		// param_surface: Option::Some(&surface.surface),
		// ..Default::default()
	// };
	let device_features = DeviceFeatures::base_presentation(&surface.surface);

	let compute_device = &mut ComputeDevice::new(&api_instance, &device_features);
	println!("Created ComputeDevice!!");

	let pool_graphics_main = compute_device.add_command_pool(&String::from("graphics"));
	println!("Created main graphics Pool!!");

	let queue_graphics_main = compute_device.get_queue(&String::from("graphics"), None).unwrap();
	println!("Got main graphics Queue!!");

	let swapchain = Swapchain::new(compute_device, &pool_graphics_main, queue_graphics_main, &surface).unwrap();
	println!("Created Swapchain!!");

	//////////////////////////// Create SwapChain Images, Views
	

	
	use rand::Rng;

	let mut rng = rand::thread_rng();

	// let img_size = (4, 4);
	let img_size = (16, 16);
	// let img_size = (32, 32, 32, 32);

	let random_noise_2d = |_| {
	    [rng.gen_range(0..=255) as u8, 
	    rng.gen_range(0..=255) as u8, 
	    rng.gen_range(0..=255) as u8, 
	    rng.gen_range(0..=255) as u8, ]
	};

	let test_img = ndarray::Array::from_shape_fn(img_size, random_noise_2d);

	println!("test_img2d random noise: {:?}", test_img);

	let mut host_img : Image<DeviceImage> = Image::from_ndarray(test_img, Format::R8G8B8A8_SRGB, 1);
	println!("created Host image!!: {:?}", host_img);
	println!("created Host image data: {:?}", host_img.data);

	let device_img = DeviceImage::new(&compute_device, pool_graphics_main.handle, queue_graphics_main.handle, &host_img);
	println!("created device_img!!");

	host_img.device_data = Some(device_img);



	let mesh_indices_raw : &[u16] = &[
	    0, 1, 2, 2, 3, 0,
	    4, 5, 6, 6, 7, 4,
	];
	// let mesh_indices_raw : &[u16] = &[
	    // 0, 1, 2, 0, 1, 2,
	    // 0, 1, 2, 0, 1, 2,
	// ];
	// let mesh_indices_raw_be: &[u8] = &[
	    // 0, 0, 0, 1, 0, 2, 0, 0, 0, 1, 0, 2, 0, 0, 0, 1, 0, 2, 0, 0, 0, 1, 0, 2
	// ];

	let mesh_vertices_raw : &[f32]= &[
	    -0.5, -0.5, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
	     0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0,
	     0.5,  0.5, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,
	    -0.5,  0.5, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0,

	    -0.5, -0.5, -0.5, 1.0, 0.0, 0.0, 1.0, 0.0,
	     0.5, -0.5, -0.5, 0.0, 1.0, 0.0, 0.0, 0.0,
	     0.5,  0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 1.0,
	    -0.5,  0.5, -0.5, 1.0, 1.0, 1.0, 1.0, 1.0,
	];

	let mesh_vertices_data : &[VertexBase3D] = unsafe{std::slice::from_raw_parts(
		mesh_vertices_raw.as_ptr() as *const VertexBase3D, 
		(mesh_vertices_raw.len() * size_of::<f32>()) / size_of::<VertexBase3D>())
	};

	let vert_shader = unsafe{ load_shader(&compute_device, &vert_path ).unwrap() };
	let frag_shader = unsafe{ load_shader(&compute_device, &frag_path ).unwrap() };

	let size = 5i32;
	let radius = 4i32;
	let capacity = (size*size) as usize;
	let mut entity_ubo_data = Vec::with_capacity(capacity);
	let mut render_components = Vec::with_capacity(capacity);
	for x in 0..size {
	    for y in 0..size {
		println!("X: {:?}, Y: {:?}", ((radius/(size-1))*x - radius/2),  ((radius/(size-1))*y - radius/2));
		let entity = SpatialComponent3D {
		    pos: [
			((radius/(size-1))*x - radius/2) as f32,
			((radius/(size-1))*y - radius/2) as f32,
			0.0 ,
			0.0
		    ],
		    rot: [0f32;16],
		    // pad: [0f32;44],
		};
		entity_ubo_data.push(entity);

		let material;
		let mut vertices = VertexBuffer::from(mesh_vertices_data);
		let mut indices = IndexBuffer::from(mesh_indices_raw);
		// indices.data = Some(mesh_indices_raw_be);
		// let mut indices_2 = VertexBuffer::from(mesh_indices_raw_be);
		// indices_2.size = indices.size;

		println!("VertexBuffer creation:");
		println!("data: {:?}", mesh_indices_raw);
		println!("data_bytes: {:?}", 1u16.to_le_bytes());
		println!("indices: {:?}", indices);

		vertices.device_data = Some(DeviceBuffer::from_data(&compute_device, 
									pool_graphics_main.handle, 
									queue_graphics_main.handle, 
									vk::BufferUsageFlags::VERTEX_BUFFER, 
									&vertices.data.unwrap()).unwrap());
		indices.device_data = Some(DeviceBuffer::from_data(&compute_device, 
									pool_graphics_main.handle, 
									queue_graphics_main.handle, 
									vk::BufferUsageFlags::INDEX_BUFFER, 
									&indices.data.unwrap()).unwrap());

		material = Material {
		    texture: &host_img,
		    vert_shader: &vert_shader,
		    frag_shader: &frag_shader,
		};
		render_components.push(RenderComponent {
		    render_type: RenderType {
			material,
			mesh: Mesh {
			    vertices,
			    indices,
			}
		    }
		});
	    }
	}
	let start_time = SystemTime::now();
	let mut world_ubo_data = WorldUBO::default();
	println!("world_ubo_data: {:?}", world_ubo_data);
	let aspect_ratio = (swapchain.extent.width as f32) / (swapchain.extent.height as f32);
	world_ubo_data.update(start_time, aspect_ratio);

	println!("world_ubo_data after update: {:#?}", world_ubo_data);

	let screen_extent = swapchain.extent;
	let render_scene;
	// must only point to data in host (if any), not own
	// data should be mutated without overhead on mutation phase
	//
	// maybe all mutable data (variable state) should be passed on each cycle as immutable
	//  we cant depend on scene from start => decouple => scene features used on start, then
	//  scene data is render arg
	println!("entity_ubo_data: {:?}", entity_ubo_data);
	println!("sizeof(entity_ubo_data::T) : {:?}", size_of::<SpatialComponent3D>());

	let min_alignment = compute_device.physical_device.properties.limits.min_uniform_buffer_offset_alignment as usize;

	let (entity_ubo, requested_memory) = unsafe {
	    HostBufferPtr::aligned_uniform_from_slice(entity_ubo_data.as_slice(), min_alignment, true)};

	let _entity_align;
	let _entity_memory;
	if requested_memory.is_some() {
	    (_entity_align, _entity_memory) = requested_memory.unwrap();
	    println!("entity ubo memory: {:?}", _entity_memory);
	}

	// TODO: make HostBuffer as slice wrapper for transparent aligned manipulation of [u8] slices.


	render_scene = RenderScene {
	    // entity_ubo: entity_ubo_data.as_slice().into(),
	    entity_ubo,
	    render_components,
	    world_ubo: HostBufferPtr::from(&world_ubo_data),
	    render_extent: [screen_extent.width, screen_extent.height],
	};
	println!("render_scene.entity_ubo.stride : {:?}", render_scene.entity_ubo.stride);

	println!("Making cube:");
	let ncube = ncube_nfaces::<VertexBase3D>(None, None);

	let renderer = Renderer::new(pool_graphics_main.handle, queue_graphics_main.handle, &swapchain, &render_scene).unwrap();

	renderer.render();

	// TODO: make rendering loop
	// let renderer_wrapped = Arc::new(renderer);
	
	let redraw_scene = false;

	let mut event_loop_handle = event_loop_wrapp.write().unwrap();
	event_loop_handle.run_return(|event, _, control_flow| {
	    *control_flow = ControlFlow::Wait;

	    // renderer_wrapped.render();
	    match event {
		Event::WindowEvent {
		    event: WindowEvent::CloseRequested 
			| WindowEvent::KeyboardInput { 
			    input: KeyboardInput {
				state: ElementState::Pressed,
				virtual_keycode: Some(VirtualKeyCode::Escape),
				..
			    }, 
			    .. },
		    ..
		} => *control_flow = ControlFlow::Exit,
		Event::RedrawRequested(_) => {
		    println!("Redraw Requested!!!");
		},
		Event::MainEventsCleared => {
		    if redraw_scene {
			world_ubo_data.update(start_time, aspect_ratio);
			let res = renderer.render();
			println!("Rendered: {:?}", res);
			println!("current time: {:?}", std::time::SystemTime::now());
		    }
		},
		_ => (),
	    }
	});

	/////////////// compute_device

	/////////////// renderer
	
	/////////////// main_loop
	
	
	// after this:
	// main_loop into Systems manager

}
