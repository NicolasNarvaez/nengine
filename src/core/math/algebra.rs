/// We base on nalgebra to generalize some transforms.

pub mod narray {
	/// To keep consistency with the ecosystem, the last index varies faster.
	#[derive(Debug)]
	pub struct ExtentVec {
		pub extent: Vec<usize>,
	}
	#[derive(Debug)]
	pub struct ExtentVecIter<'a> {
		pub extent: &'a ExtentVec,
		pub value: Vec<usize>,
	}

	impl ExtentVec {
		pub fn new(val: &Vec<usize>) -> Self {
			ExtentVec {
				extent: val.clone()
			}
		}

		pub fn iter<'a>(&'a self) -> ExtentVecIter<'a> {
			ExtentVecIter { extent: &self, value: vec![0usize; self.extent.len()] }
		}
	     pub fn is_last(&self, value: &Vec<usize>) -> bool {
		let length = self.extent.len();
		for component in 0..length { if value[component] != (self.extent[component] - 1) {return false}; }
		return true;
	     }
	}

	impl<'a> ExtentVecIter<'a> {
		/// Returns linear index in extent
	    fn index_of(&self, value: Vec<usize>) -> usize {
		let mut index = 0usize;
		let mut pad = 1;

		for i in 0..self.extent.extent.len() {
			index += pad*self.value[i];
			pad *= self.extent.extent[i];
		}
		index
	    }
	}

	impl<'a> Iterator for ExtentVecIter<'a> {
    type Item = Vec<usize>;

    fn next(&mut self) -> Option<Self::Item> {
	let length = self.value.len();
	if self.value[length-1] == self.extent.extent[length-1] {return None;}

	let current_value = Some(self.value.clone());

	self.value[0] += 1;
	for i in 0..length {
		if self.value[i] == self.extent.extent[i] && i != length-1 {
			self.value[i] = 0; 
			self.value[i+1] += 1; 
		}
		else {break;}
	}

	current_value
    }
}

}

pub mod transform {
	/// makes a n-d generalized perspective projection matrix
	// first by inspection, iterative partial generalization/compression
	pub fn nperspective_matrix(n: u8,) {
	}
	/// makes a n-d generalized perspective projection vector
	pub fn nperspective() {
	}

	/// makes a n-d generalized rotation matrix
	pub fn nrotation_matrix() {}

	pub trait NDRotation {
		fn from_basis_plane() {
		}
	}
}
