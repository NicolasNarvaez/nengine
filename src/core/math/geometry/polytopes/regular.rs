use std::{mem::size_of, fmt::Debug, ops::Div};
use num::{One, FromPrimitive};

use crate::core::{compute::resources::buffers::{PosAttrib, TexAttrib, VertexTypeInfo}, math::algebra::narray::{ExtentVecIter, ExtentVec}};


/// TODO: Evaluate generic_const_exprs (we need expressions over dim for diverse dimentional features/reprs) and 
/// compile time optimization of vec, [], simd (w/wo nalgebra).

pub struct Cylinder {
}


/// A n-1 surface, regular n-1-cubic honeycomb, simplex triangulated (all features are simplexes), single cubic face 
/// (all cubes share same text coords).
// pub fn nmesh

/// Produces a cube of polytope faces (not made by simplexes), of N-1 dimensions, mapping them to
/// textures on 3 modes (nface, 3face, 1face) of repetition for tex compression.
/// Here, for lazyness, we consider the face texture polygon as convex, but not assume the memory order as correct. 
/// Currently n-cubes memory order follows the extent(2^n) iterator order.
/// (TODO: add convex cube features iter.)
///
/// Implementation details:
/// We use a different fn depending on texture mapping, nfaces gives every face a separate space,
/// 3face merges all horizontal faces, leaving 2 remaining vertical faces, 1face makes all faces
/// share the same coords.
/// T must be repr(c), UNSAFE (raw pointer fields traversal)
///
// ncube_nfaces, ncube_3faces, ncube_1face
// TODO: scale uv mapping coords with side param (wrapping) & add generation features/options
// TODO: (rectangles only) allow alternative encoding for textures as 2-point BB instead of 2^n.
pub fn ncube_nfaces<T: VertexTypeInfo + PosAttrib + TexAttrib + Debug>(
	data: Option<Box<[T]>>, 
	radius: Option<Vec::<<T as PosAttrib>::Format>>
	)
-> Option<Box<[T]>>  
where <T as PosAttrib>::Format : PartialEq + std::ops::Mul + One + Copy
		+ std::ops::Neg<Output = <T as PosAttrib>::Format> //(Output : <T as PosAttrib>::Format) ,
	, <T as TexAttrib>::Format : One + FromPrimitive + Debug + Copy + std::ops::Add<Output = <T as TexAttrib>::Format>
		+ Div<Output = <T as TexAttrib>::Format>
	// , <<T as PosAttrib>::Format as Neg>::Output : T::Format

// std::ops::{Add, Sub, Mul, Div}
{
	let radius = radius.unwrap_or_else(|| vec![<T as PosAttrib>::Format::one(); T::DIM]);

	if radius.len() != T::DIM {panic!("NCube side has incorrect dimension")};

	let vertex_n = T::DIM * 2 * (2_u32.pow((T::DIM - 1) as u32) as usize);

	let mut data_vector = Vec::with_capacity(vertex_n); unsafe {data_vector.set_len(vertex_n);}
	let data : Box<[T]> = data_vector.into_boxed_slice();
	let data_ptr = data.as_ptr();
	println!("data len {:?}", data.len());

	// let value = data.as_ref().unwrap().as_ref();

	// let nindex : Vec::<<T as PosAttrib>::Format> = Vec::<<T as PosAttrib>::Format>::with_capacity(T::DIM);
	// let nindex : Vec::<<T as PosAttrib>::Format> = vec![0.into(); T::DIM];
	let mut nindex = vec![0; T::DIM + 1]; // "extra index stores global loop condition"

	// we build each face in order (dimension, orientation), mapping textures in texture ordered
	// lattice mantaining regular proportions (regular cube) (texture_order(face_order) -> vec_N-1_position {
	// max ceil(root(faces, n-1))^(n-1) faces})

	let faces_n = T::DIM*2;
	let texmap_extent_len = if T::DIM < 4 {T::DIM -1} else {f64::log2(faces_n as f64).ceil() as usize} ; 
	let mut texmap_extent = vec![0usize, texmap_extent_len]; 
	let mut texmap_steps : Vec<<T as TexAttrib>::Format> = Vec::with_capacity(texmap_extent_len);
	unsafe {texmap_extent.set_len(texmap_extent_len);}
	unsafe {texmap_steps.set_len(texmap_extent_len);}

	if T::DIM == 3 {
		texmap_extent[0] = 3;
		texmap_extent[1] = 2;
		texmap_steps[0] = <T as TexAttrib>::Format::from_u32(1).unwrap()/<T as TexAttrib>::Format::from_u32(3).unwrap();
		texmap_steps[1] = <T as TexAttrib>::Format::from_u32(1).unwrap()/<T as TexAttrib>::Format::from_u32(2).unwrap();
	}
	else if T::DIM == 2 { // For Completeness
		texmap_extent[0] = 4;
		texmap_steps[0] = <T as TexAttrib>::Format::from_u32(1).unwrap()/<T as TexAttrib>::Format::from_u32(4).unwrap();
	}
	else {
		
		for i in 0..texmap_extent_len {
			texmap_extent[i] = 2;
			texmap_steps[i] = <T as TexAttrib>::Format::from_u32(1).unwrap()/<T as TexAttrib>::Format::from_u32(2).unwrap();
		}
	}

	let texmap_extent_wrapper = ExtentVec::new(&texmap_extent);
	let mut texmap_extent_iter = texmap_extent_wrapper.iter();
	let mut current_vert_index = 0usize;

	println!("T::DIM: {:?} ", T::DIM);
	println!("texmap_steps: {:?}", texmap_steps);
	println!("texmap_extent_iter {:?}", texmap_extent_iter);

	// We take care of face orientation when computing the generalized cross product, for this we negate instead 
	// of rotate the vectors to match the cross product determinant:
	//	On even (starting at 0) axis  (even axis lost: 0 axis (X) would be if face_i in {0, 1}), we leave vectors unchanged
	//	In odd axis, we invert the 0 (X) point
	//	For any axis, its oposite (odd index: face_i = 1, 3, ..) inverts the 0(X) (or 1(Y) if face_i = 1) point.
	for face_i in 0..T::DIM*2 {
		let current_dim = face_i/2;
		let axis_orientation = face_i%2 == 1; // true = Back face
		let cross_orientation = current_dim%2 == 1; // true => inverted normal
		let inverse_component = if current_dim == 0 {1} else {0};
		let inverse_component_orientation = axis_orientation ^ cross_orientation;
		println!("face_i: {:?}, current_dim: {:?}, orientation: {:?}, cross_orientation: {:?}", 
				 face_i, current_dim, axis_orientation, cross_orientation);

		let texmap_pos : Vec<<T as TexAttrib>::Format> = texmap_extent_iter.next().unwrap().into_iter()
			.map(|component| <T as TexAttrib>::Format::from_usize(component).unwrap()).collect();

		// n-1 face points (n-1)-cube + origin
		// We only use the center (0)
		let face_extent = vec![2_usize; T::DIM-1];
		let face_extent_wrapper = ExtentVec::new(&face_extent);
		'vert_iter: for vert_pos in face_extent_wrapper.iter() {
			// ignore all not intersected with axises
			let mut surfaced = 0;
			for comp in vert_pos.iter() {
				if *comp == 2_usize {surfaced += 1}
				if surfaced == 2 {break 'vert_iter}
			}

			let vertex_ptr = (data_ptr as usize) + current_vert_index*size_of::<T>();
			println!("current_vert_index {:?}", current_vert_index);
			println!("vert_pos {:?}", vert_pos);
			println!("data_ptr {:p}", data_ptr);
			println!("vertex_ptr {:?}", vertex_ptr);

			// n components
			for component in 0..T::DIM {
				let tex_component = component - if component >= (T::DIM + 1)/2 {1} else {0};
				println!("component {:?}", component);
				println!("tex_component {:?}", tex_component);

				let component_ptr = (vertex_ptr + <T as PosAttrib>::OFFSET 
						+ (component * size_of::<<T as PosAttrib>::Format>())
					) as *mut <T as PosAttrib>::Format;
				let tex_ptr = (vertex_ptr + <T as TexAttrib>::OFFSET
						+ (tex_component * size_of::<<T as TexAttrib>::Format>())
					) as *mut <T as TexAttrib>::Format;

				println!("component_ptr {:?}", component_ptr);
				println!("tex_ptr {:?}", tex_ptr);

				if component == current_dim {
					unsafe { *component_ptr = if axis_orientation {radius[current_dim]} else {-radius[current_dim]}; }
				}
				else {
					// let local_position = if component == inverse_component {1} else {0};
					let local_origin = if (component == inverse_component) && inverse_component_orientation {1} else {0};
					unsafe { *component_ptr = 
							if vert_pos[tex_component] == local_origin {-radius[current_dim]} else {radius[current_dim]}; 
					}

					unsafe {
						let texmap_step = texmap_pos[tex_component];
						let face_step = <T as TexAttrib>::Format::from_usize(vert_pos[tex_component]).unwrap();
						*(tex_ptr) = (texmap_step*texmap_steps[tex_component]) + (face_step*texmap_steps[tex_component]);
					}
				}
			}
			current_vert_index += 1;
		}
		println!("data: {:?}", data);
	}
	

	// (these are fundamental extent ops)
	// while nindex still not full
	// use nindex: 
	//		for each component, map nindex*side + pos
	// increase
	// while nindex[T::DIM] != 1.into() {
//
		// for i in 0..T::DIM {
//
			// // calc vertex n
			// let pos : usize = 2usize.pow(i as u32); // sum all
//
			// // opnly traits can access generic struct layout => lets edit data directly (raw offsets)
			// value[pos].pos[i] = side*nindex[i].into();
			//
//
			// if nindex[i] == 0 {
				// nindex[i] = 1;
				// break;
			// }
			// else {
				// nindex[i] = 0;
			// }
		// }
	// }


	Some(data)
}
