
// Regular polytopes, tessellations, common surfaces, and some generalized objects.
//

pub mod regular;
pub mod surfaces;
pub mod objects;
