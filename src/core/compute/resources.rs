
// Owns memory backed image, in the host and gpu.
// Useful for single images that will be sent to gpu and might be used in cpu.

use std::{mem::size_of, os::raw::c_void, marker::PhantomData};

use ndarray::{Array, Dimension};

use super::vulkan::{self, resources::RenderTypeS};

pub type Extent = Box<[usize]>;

pub use image::Image;
pub use format::{FormatTypeVendorMap, Format};

/// Because Align implies a bigger buffer should/might be allocated, it creates its own buffer, this will
/// probably change favouring externally managed memory.
/// Tentative: new() builds with ptr, while from_data() returns a created slice
#[derive(Debug)]
pub struct Align<T> {
    ptr: *mut c_void,
    size: usize,
    elem_size: usize,
    _m: PhantomData<T>,
}

pub struct AlignIterMut<'a, T> {
    align: &'a mut Align<T>,
    current: usize,
}

impl<T> Align<T> {
    pub unsafe fn new(dst: *mut c_void, src: &[T], min_alignment: usize) -> Self {
	let elem_size = Self::pad_min_byte_size(size_of::<T>(), min_alignment);
	let size = elem_size * src.len();

	Self {
	    ptr: dst,
	    size,
	    elem_size,
	    _m: PhantomData,
	}
    }

    // Creates new buffer based on src.
    pub unsafe fn from_slice(src: &[T], min_alignment: usize) -> (Self, Box<[u8]>) {
	let elem_size = Self::pad_min_byte_size(size_of::<T>(), min_alignment);
	let size = elem_size * src.len();

	let mut dst = vec![0u8; size].into_boxed_slice();

	(Self::new(dst.as_mut_ptr() as *mut c_void, src, min_alignment), dst)
    }

    pub fn iter_mut(&mut self) -> AlignIterMut<T> {
	AlignIterMut { align: self, current: 0 }
    }

    pub fn pad_min_byte_size(element_alignment: usize, min_alignment: usize) -> usize {
	    if element_alignment == 0 {return min_alignment};

	    (min_alignment + element_alignment - 1) & !(element_alignment-1)
    }

    pub fn slice_requires_padding(slice: &[T], min_alignment: usize) -> usize {
	let value_size = size_of::<T>();
	let padded = Self::pad_min_byte_size(value_size, min_alignment);

	if padded == value_size {0}
	else {padded}
    }
}

impl<T: Copy> Align<T> {
    pub fn copy_from_slice(&mut self, slice: &[T]) {
	let src_size = size_of::<T>();
	let src_len = src_size * slice.len();

	if self.size < src_len {panic!(
	    "Align wrapper not big enough!, requested: {:?}, available: {:?}", src_size, src_len );}

	if self.elem_size == src_size {
	    let src = unsafe { std::slice::from_raw_parts(slice.as_ptr() as *const u8, src_len) };
	    let dst = unsafe { std::slice::from_raw_parts_mut(self.ptr as *mut u8, src_len) };
	    dst.copy_from_slice(src);
	}
	else {
	    for (i, val) in self.iter_mut().enumerate().take(slice.len()) {
		*val = slice[i]
	    }
	}
    }
}

impl <'a, T: 'a> Iterator for AlignIterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {

	if self.current == self.align.size { return None; }

	unsafe {
	    let ptr = self.align.ptr.offset(self.current as isize).cast();
	    self.current += self.align.elem_size;
	    Some(&mut *ptr)
	}
    }
}

pub mod image {
    use std::{mem::size_of, os::raw::c_void};

    use derivative::Derivative;
    use ndarray::{Dimension, Array};

    use super::{Extent, Format};

    #[derive(Derivative)]
    #[derivative(Debug)]
    pub struct Image<T> {
	pub path: Option<String>,
	pub data: Box<[u8]>,
	#[derivative(Debug="ignore")]
	pub device_data: Option<T>,

	pub extent: Extent,
	pub format: Format,
	pub channels: u32,
    }

    impl<T> Image<T> {
	// if path, loads
	pub fn new(data: Box<[u8]>, extent: Extent, format: Format, channels: u32) 
	-> Self {
	    println!("Creating Host image!!!");
	    Self {
		path: None,
		data, 
		device_data: None,
		extent,
		format,
		channels,
	    }
	}

	pub fn from_ndarray<F: Clone + std::fmt::Debug, D: Dimension>(data: Array<F, D>, format: Format, channels: u32) -> Self {

	    let extent = data.shape().into();

	    let size_bytes = data.len() * size_of::<F>();
	    let as_slice = data.as_slice();

	    let as_slice = as_slice.unwrap();
	    let data_ptr = as_slice.as_ptr() as *mut u8;
	    let src_slice = unsafe {std::slice::from_raw_parts_mut(data_ptr, size_bytes)};
	    let data = src_slice.to_vec().into_boxed_slice();

	    Self {
		path: None,
		data,
		device_data: None,
		extent,
		format,
		channels,
	    }
	}

	// pub fn from_file(path: String) -> Self {
	    // let data = Array<F, D>
	    // Self {
		// data,
		// path,
	    // }
	// }
    }
}


// Some structs use a RenderTypeT type to extract vendor type data
pub mod scene {
    use super::{Image, buffers};

    pub struct ShaderModule<T> {
	pub path: String,
	pub code: String,
	pub module: Option<T>,
    }

    pub struct Material<'a, T, S> {
	pub texture: &'a Image<T>,
	pub frag_shader: &'a ShaderModule<S>,
	pub vert_shader: &'a ShaderModule<S>,
    }

    pub struct Mesh<'a, T> {
	pub vertices: buffers::VertexBuffer<'a, T>,
	pub indices: buffers::IndexBuffer<'a, T>,
    }

    pub struct RenderType<'a, T: RenderTypeT> {
	pub material: Material<'a, T::Image, T::ShaderModule>,
	pub mesh: Mesh<'a, T::Buffer>,
    }

    pub trait RenderTypeT {
	type Image;
	type Buffer;
	type ShaderModule;
    }
}

// pub type Scene_Object<'a> = SceneObject<'a, SceneObjectS<'a>>;

pub mod buffers {
    use std::{mem::size_of, os::raw::c_void, marker::PhantomData};

    use derivative::Derivative;
    use memoffset::offset_of;

    use super::{Format, format::{IndexFormat, IndexFormatType}, Align};

    pub struct VertexAttribute {
	// pub format:
    }

    #[derive(Clone, Debug)]
    pub struct VertexInputAttribute {
	pub format: Format,
	pub offset: usize,
    }

    // Extracts vertex attributes for render pipeline 
    pub trait VertexInputAttributes {
	fn get_attributes() -> Vec<VertexInputAttribute>;
    }

    // RenderScene input
    #[derive(Clone, Debug)]
    pub struct VertexInputDescription { // TODO: rename to VertexDescription
	pub stride: usize,
	pub attributes: Vec<VertexInputAttribute>,
    }

    impl VertexInputDescription {
	pub fn from_slice<T: VertexInputAttributes>(_buffer: &[T]) -> Self {
	    Self {
		stride: size_of::<T>(),
		attributes: T::get_attributes(),
	    }
	}
	pub fn from_vector<T: VertexInputAttributes>(_buffer: Vec<T>) -> Self {
	    Self {
		stride: size_of::<T>(),
		attributes: T::get_attributes(),
	    }
	}
    }

    // size: number of elements
    #[derive(Clone)]
    #[derive(Derivative)]
    #[derivative(Debug)]
    pub struct VertexBuffer<'a, T> {
	pub data: Option<&'a [u8]>,
	#[derivative(Debug="ignore")]
	pub device_data: Option<T>,
	pub size: usize,
	pub vertex_description: VertexInputDescription,
    }

    impl<'a, T, V: VertexInputAttributes> From<&'a [V]> for VertexBuffer<'a, T> {
        fn from(value: &[V]) -> Self {
	    let size_bytes = value.len() * size_of::<V>();
	    let data = unsafe{std::slice::from_raw_parts(value.as_ptr() as *const u8, size_bytes)}.as_ref();
	    Self { 
		data: Some(data), 
		device_data: None, 
		size: value.len(), 
		vertex_description: VertexInputDescription::from_slice(value) }
        }
    }

    #[derive(Clone)]
    #[derive(Derivative)]
    #[derivative(Debug)]
    pub struct IndexBuffer<'a, T> {
	pub data: Option<&'a [u8]>,
	#[derivative(Debug="ignore")]
	pub device_data: Option<T>,
	pub size: usize,
	pub index_type: IndexFormat,
    }

    impl<'a, V: IndexFormatType, T> From<&'a [V]> for IndexBuffer<'a, T> {
        fn from(value: &'a [V]) -> Self {
	    let index_type = V::get_format();
	    let size_bytes = value.len() * size_of::<V>();
	    let data = unsafe{std::slice::from_raw_parts(value.as_ptr() as *const u8, size_bytes)}.as_ref();

	    Self { 
		data: Some(data), 
		device_data: None, 
		size: value.len(), 
		index_type,
	    }
        }
    }

    // TODO: make a HostBuffer Slice wrapper to optimize uniform buffer data creation (avoid extra
    // padding step)

    // host resource descriptor
    pub struct HostBufferPtr {
	pub stride: usize,
	pub size: usize,
	pub data: *const u8,
    }

    impl<'a, T> From<&'a [T]> for HostBufferPtr {
	fn from(value: &'a [T]) -> Self {
	    let stride = size_of::<T>();
	    let size_bytes = value.len() * stride;
	    let data = value.as_ptr() as *const u8;
	    Self { stride, size: size_bytes, data, }
	}
    }

    impl HostBufferPtr {
	pub fn from<T>(value: &T) -> Self {
	    let stride = size_of::<T>();
	    let data = value as *const T as *const u8;
	    Self { stride, size: stride, data }
	}

	pub unsafe fn aligned_uniform_from_slice<T: Copy + std::fmt::Debug>(value: &[T], min_alignment: usize, copy: bool) 
	-> (Self, Option<(Align<T>, Box<[u8]>)>) {

	    let padded = Align::slice_requires_padding(value, min_alignment);
	    if padded != 0 {
		let (mut align, data) = Align::from_slice(value, min_alignment);

		if copy { align.copy_from_slice(value); }

		(Self {
		    data: data.as_ptr(),
		    stride: padded,
		    size: align.size,
		}, Some((align, data))
		)
	    }
	    else {
		(value.into(), None)
	    }
	}
    }
    // WTF
    // impl<'a, T: Sized> From<T> for HostBuffer<'a> {
	// fn from(value: T) -> Self {
	    // let stride = size_of::<T>();
	    // let data = unsafe{std::slice::from_raw_parts(value.as_ptr() as *const u8, stride)}.as_ref();
	    // Self { stride, data }
	// }
    // }
    
    impl IndexFormatType for u8 { fn get_format() -> IndexFormat { IndexFormat::U8 } }
    impl IndexFormatType for u16 { fn get_format() -> IndexFormat { IndexFormat::U16 } }
    impl IndexFormatType for u32 { fn get_format() -> IndexFormat { IndexFormat::U32 } }

    //////////// Experiments on comptime vertex struct layout

    // pub struct VertexBaseDyn {
	// pub pos:
    // }
    // as_typed_slice() -> T
    // #[repr(C)]
    // pub struct SliceWrapper<T, const N: usize> {
	// value: [T; N],
    // }
    // pub struct VertexBaseND_<const N: usize> {
	// pub pos: SliceWrapper<f32, {N}>,
	// pub color: SliceWrapper<f32, {N-1}>,
    // }
    // const fn substract_one(length: usize) -> usize { length -1 }
    // pub struct VertexBaseND<const N: usize> {
	// pub pos: [f32;N],
	// pub color: [f32;{substract_one(N)}],
	// pub n: usize,
	// pub tex_coord: [f32;text_dim(N, 1)],
    // }
    // impl<const N:usize> VertexBaseND<N> {
	// pub fn new() -> Self {
	    // Self {
		// pos: [],
		// color: [],
		// n: N,
	    // }
	// }
    // }
    // pub const fn text_dim(n: usize, m: usize) {n-m}

    pub trait Zero {
	fn zero() -> Self;
    }

    impl Zero for u32 { fn zero() -> Self { 0u32 } }


    pub trait VertX {
    }
    pub trait VAPos {
	const offset: u32;
	const len: usize;
	type Whole;
	type Format: Zero + Copy;
    }
    impl<T: VertX>  VAPos for T {
	const offset : u32 = 5;
	const len: usize = 10;
	type Whole = [f32;4];
	type Format = u32;
    }
    struct NewVT {
    }
    // pub fn b<T: VAPos>() -> T::Format {
    // pub fn b<T: VAPos>() -> T::Format
    // {
	// 40 as T::Format
	// let a = f32::ZERO;
	// let a = [T::Format::zero(); T::len];
	// return a;
    // }

    pub fn a () {
    }


    //  TODO macro impl for vendor format
    #[repr(C)]
    #[derive(Debug)]
    pub struct VertexBase3D {
	pub pos: [f32;3],
	pub color: [f32;3],
	pub tex: [f32;2],
    }
    #[repr(C)]
    pub struct VertexBase4D {
	pub pos: [f32;4],
	pub color: [f32;4],
	pub tex: [f32;3],
    }
    #[repr(C)]
    pub struct VertexBase5D {
	pub pos: [f32;5],
	pub color: [f32;5],
	pub tex: [f32;4],
    }
    #[repr(C)]
    pub struct VertexBase6D {
	pub pos: [f32;6],
	pub color: [f32;6],
	pub tex: [f32;5],
    }


    /// There is no much meaning on storing graphics features (faces) as vertex lists: 
    ///	We render using rays, so we need to get inside the feature 
    ///	aka: obtain the feature transform to its inner space and invert it, this oposes
    ///	completely to the traditional rasterization constraints, which implied trangle lists as
    ///	optimal reprs thansk to only raster a plane using line bounds described by point pairs.
    ///
    ///	So we store features in two ways (currently): 
    ///     Raw shape ("Points"): For dynamic changing meshes: slower rendering, faster manipulation
    ///		feature points (n n-points) + normal(n) + texture mapping points (n n-1-points)
    ///		[+ normal per point (for shading)]
    ///     Inverted model transform: For static meshes: fastest rendering, very slow manipulation 
    ///		matrix(n-1, n) (from n to n-1) + origin + normal(n) + texture mapping points n n-1-points)
    ///		[+ normal per point (for shading)]
    ///
    ///	Cube, Simplex and SimplexPrism occupy the same representation, changing only in logic.
    ///
    /// We take care of face orientation when computing the generalized cross product, for this we negate instead 
    /// of rotate the vectors to match the cross product determinant:
    ///	    On even (starting at 0) axis  (even axis lost: 0 axis (X) would be if face_i in {0, 1}), we leave vectors unchanged
    ///	    In odd axis, we invert the 0 (X) point
    ///	    For any axis, its oposite (odd index: face_i = 1, 3, ..) inverts the 0(X) (or 1(Y) if axis = 0) point.
    ///
    ///	TODO: think on how to optimize "surface rays" (our ray spawns a n-2 dim integration surface).
    #[repr(C)]
    pub struct RenderFeatureFaceCube4DPoints {
    }
    #[repr(C)]
    pub struct RenderFeatureFaceCube4DInverseTransform {
    }
    #[repr(C)]
    pub struct RenderFeatureFaceSimplex4DPoints {
    }
    #[repr(C)]
    pub struct RenderFeatureFaceSimplex4DInverseTransform {
    }
    #[repr(C)]
    pub struct RenderFeatureFaceSimplexPrism4DInverseTransform {
    }

    impl VertexInputAttributes for VertexBase3D {
	fn get_attributes() -> Vec<VertexInputAttribute> {
	    vec![ 
		VertexInputAttribute { 
		    // format: <<[f32;3] as CompositeFormat>::Repr as VendorFormat>::get_format(),
		    format: Format::R32G32B32_SFLOAT,
		    offset: offset_of!(VertexBase3D, pos),
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32_SFLOAT,
		    offset: offset_of!(VertexBase3D, color),
		},
		VertexInputAttribute { 
		    format: Format::R32G32_SFLOAT,
		    offset: offset_of!(VertexBase3D, tex),
		},
	    ]
	}
    }
    impl VertexInputAttributes for VertexBase4D {
	fn get_attributes() -> Vec<VertexInputAttribute> {
	    vec![ 
		VertexInputAttribute { 
		    format: Format::R32G32B32A32_SFLOAT,
		    offset: offset_of!(VertexBase4D, pos),
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32A32_SFLOAT,
		    offset: offset_of!(VertexBase4D, color),
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32_SFLOAT,
		    offset: offset_of!(VertexBase4D, tex),
		},
	    ]
	}
    }
    pub trait VertexTypeInfo {
	const DIM: usize;
    }
    pub trait VertexTypeInfoExt : VertexTypeInfo {
	// const text_arr: [usize; <Self as VertexTypeInfo>::DIM];
    }
    // type ExtArray<S:
    pub trait PosAttrib {
	const OFFSET: usize;
	type Format: Clone;
    }
    pub trait TexAttrib {
	const OFFSET: usize;
	type Format: Clone;
    }

    impl PosAttrib for VertexBase3D { const OFFSET : usize = offset_of!(VertexBase3D, pos); type Format = f32; }
    impl TexAttrib for VertexBase3D { const OFFSET : usize = offset_of!(VertexBase3D, tex); type Format = f32; }
    impl VertexTypeInfo for VertexBase3D { const DIM: usize = 3; }

    impl PosAttrib for VertexBase4D { const OFFSET : usize = offset_of!(VertexBase4D, pos); type Format = f32; }
    impl TexAttrib for VertexBase4D { const OFFSET : usize = offset_of!(VertexBase4D, tex); type Format = f32; }
    impl VertexTypeInfo for VertexBase4D { const DIM: usize = 4; }

    impl VertexInputAttributes for VertexBase5D {
	fn get_attributes() -> Vec<VertexInputAttribute> {
	    vec![ 
		VertexInputAttribute { 
		    format: Format::R32G32B32_SFLOAT,
		    offset: offset_of!(VertexBase5D, pos),
		},
		VertexInputAttribute { 
		    format: Format::R32G32_SFLOAT,
		    offset: offset_of!(VertexBase5D, pos) + 3*4,
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32_SFLOAT,
		    offset: offset_of!(VertexBase5D, color),
		},
		VertexInputAttribute { 
		    format: Format::R32G32_SFLOAT,
		    offset: offset_of!(VertexBase5D, color) + 3*4,
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32A32_SFLOAT,
		    offset: offset_of!(VertexBase5D, tex),
		},

	    ]
	}
    }
    impl VertexInputAttributes for VertexBase6D {
	fn get_attributes() -> Vec<VertexInputAttribute> {
	    vec![ 
		VertexInputAttribute { 
		    format: Format::R32G32B32A32_SFLOAT,
		    offset: offset_of!(VertexBase6D, pos),
		},
		VertexInputAttribute { 
		    format: Format::R32G32_SFLOAT,
		    offset: offset_of!(VertexBase6D, pos) + 4*4,
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32A32_SFLOAT,
		    offset: offset_of!(VertexBase6D, color),
		},
		VertexInputAttribute { 
		    format: Format::R32G32_SFLOAT,
		    offset: offset_of!(VertexBase6D, color) + 4*4,
		},
		VertexInputAttribute { 
		    format: Format::R32G32B32_SFLOAT,
		    offset: offset_of!(VertexBase6D, tex),
		},
		VertexInputAttribute { 
		    format: Format::R32G32_SFLOAT,
		    offset: offset_of!(VertexBase6D, tex) + 3*4,
		},

	    ]
	}
    }
}

// Encodes type mappings from formats into encodings
#[allow(non_camel_case_types)]
pub mod format {
    // pub trait CompositeFormat {
	// type Repr;
    // }
    // TODO: macro to map slices to repr
    // impl CompositeFormat for [f32;3] {
	// // type Repr = [format::R32G32B32_SFLOAT; 0];
	// type Repr = (format::R32G32B32_SFLOAT);
    // }
    // impl CompositeFormat for [f32;4] {
	// type Repr = (format::R32G32_SFLOAT, format::R32G32_SFLOAT);
    // }



    // Type mappers
    pub trait IndexFormatType {
	fn get_format() -> IndexFormat;
    }

    pub trait FormatTypeVendorMap<VFormat> {
	fn get_format() -> VFormat;
    }

    // Type enums
    // #[allow(non_camel_case_types)]
    // #[derive(Clone, Debug)]
    // pub enum HostFormat {
	//
    // }

    #[allow(non_camel_case_types)]
    #[derive(Clone, Debug)]
    pub enum IndexFormat {
	U8,
	U16,
	U32,
    }

    #[allow(non_camel_case_types)]
    #[derive(Clone, Debug)]
    pub enum Format {
	UNDEFINED,
	R8G8B8A8_SRGB,
	R32G32_SFLOAT,
	R32G32B32_SFLOAT,
	R32G32B32A32_SFLOAT,
    }

    // Type mapping helper for Format enum into associated types
    // FormatTypeVendorMap maps into vendor specific format runtime value
    pub trait FormatType {
	type Repr;
    }

    pub struct UNDEFINED ();
    impl FormatType for UNDEFINED {
	type Repr = ();
    }
    pub struct R8G8B8A8_SRGB ();
    impl FormatType for R8G8B8A8_SRGB {
	type Repr = [u8;4];
    }
    pub struct R32G32_SFLOAT ();
    impl FormatType for R32G32_SFLOAT {
	type Repr = [f32;2];
    }
    pub struct R32G32B32_SFLOAT ();
    impl FormatType for R32G32B32_SFLOAT {
	type Repr = [f32;3];
    }
    pub struct R32G32B32A32_SFLOAT ();
    impl FormatType for R32G32B32A32_SFLOAT {
	type Repr = [f32;4];
    }

    // // , FORMAT_R32G32B32A32_UINT = 107
    // // , FORMAT_R32G32B32A32_SINT = 108
    // // , FORMAT_R32G32B32A32_SFLOAT = 109
// pub enum Format {
    // UNDEFINED,
    // // , FORMAT_R4G4_UNORM_PACK8 = 1
    // // , FORMAT_R4G4B4A4_UNORM_PACK16 = 2
    // // , FORMAT_B4G4R4A4_UNORM_PACK16 = 3
    // // , FORMAT_R5G6B5_UNORM_PACK16 = 4
    // // , FORMAT_B5G6R5_UNORM_PACK16 = 5
    // // , FORMAT_R5G5B5A1_UNORM_PACK16 = 6
    // // , FORMAT_B5G5R5A1_UNORM_PACK16 = 7
    // // , FORMAT_A1R5G5B5_UNORM_PACK16 = 8
    // // , FORMAT_R8_UNORM = 9
    // // , FORMAT_R8_SNORM = 10
    // // , FORMAT_R8_USCALED = 11
    // // , FORMAT_R8_SSCALED = 12
    // // , FORMAT_R8_UINT = 13
    // // , FORMAT_R8_SINT = 14
    // // , FORMAT_R8_SRGB = 15
    // // , FORMAT_R8G8_UNORM = 16
    // // , FORMAT_R8G8_SNORM = 17
    // // , FORMAT_R8G8_USCALED = 18
    // // , FORMAT_R8G8_SSCALED = 19
    // // , FORMAT_R8G8_UINT = 20
    // // , FORMAT_R8G8_SINT = 21
    // // , FORMAT_R8G8_SRGB = 22
    // // , FORMAT_R8G8B8_UNORM = 23
    // // , FORMAT_R8G8B8_SNORM = 24
    // // , FORMAT_R8G8B8_USCALED = 25
    // // , FORMAT_R8G8B8_SSCALED = 26
    // // , FORMAT_R8G8B8_UINT = 27
    // // , FORMAT_R8G8B8_SINT = 28
    // // , FORMAT_R8G8B8_SRGB = 29
    // // , FORMAT_B8G8R8_UNORM = 30
    // // , FORMAT_B8G8R8_SNORM = 31
    // // , FORMAT_B8G8R8_USCALED = 32
    // // , FORMAT_B8G8R8_SSCALED = 33
    // // , FORMAT_B8G8R8_UINT = 34
    // // , FORMAT_B8G8R8_SINT = 35
    // // , FORMAT_B8G8R8_SRGB = 36
    // // , FORMAT_R8G8B8A8_UNORM = 37
    // // , FORMAT_R8G8B8A8_SNORM = 38
    // // , FORMAT_R8G8B8A8_USCALED = 39
    // // , FORMAT_R8G8B8A8_SSCALED = 40
    // // , FORMAT_R8G8B8A8_UINT = 41
    // // , FORMAT_R8G8B8A8_SINT = 42
    // R8G8B8A8_SRGB,
    // // , FORMAT_B8G8R8A8_UNORM = 44
    // // , FORMAT_B8G8R8A8_SNORM = 45
    // // , FORMAT_B8G8R8A8_USCALED = 46
    // // , FORMAT_B8G8R8A8_SSCALED = 47
    // // , FORMAT_B8G8R8A8_UINT = 48
    // // , FORMAT_B8G8R8A8_SINT = 49
    // // , FORMAT_B8G8R8A8_SRGB = 50
    // // , FORMAT_A8B8G8R8_UNORM_PACK32 = 51
    // // , FORMAT_A8B8G8R8_SNORM_PACK32 = 52
    // // , FORMAT_A8B8G8R8_USCALED_PACK32 = 53
    // // , FORMAT_A8B8G8R8_SSCALED_PACK32 = 54
    // // , FORMAT_A8B8G8R8_UINT_PACK32 = 55
    // // , FORMAT_A8B8G8R8_SINT_PACK32 = 56
    // // , FORMAT_A8B8G8R8_SRGB_PACK32 = 57
    // // , FORMAT_A2R10G10B10_UNORM_PACK32 = 58
    // // , FORMAT_A2R10G10B10_SNORM_PACK32 = 59
    // // , FORMAT_A2R10G10B10_USCALED_PACK32 = 60
    // // , FORMAT_A2R10G10B10_SSCALED_PACK32 = 61
    // // , FORMAT_A2R10G10B10_UINT_PACK32 = 62
    // // , FORMAT_A2R10G10B10_SINT_PACK32 = 63
    // // , FORMAT_A2B10G10R10_UNORM_PACK32 = 64
    // // , FORMAT_A2B10G10R10_SNORM_PACK32 = 65
    // // , FORMAT_A2B10G10R10_USCALED_PACK32 = 66
    // // , FORMAT_A2B10G10R10_SSCALED_PACK32 = 67
    // // , FORMAT_A2B10G10R10_UINT_PACK32 = 68
    // // , FORMAT_A2B10G10R10_SINT_PACK32 = 69
    // // , FORMAT_R16_UNORM = 70
    // // , FORMAT_R16_SNORM = 71
    // // , FORMAT_R16_USCALED = 72
    // // , FORMAT_R16_SSCALED = 73
    // // , FORMAT_R16_UINT = 74
    // // , FORMAT_R16_SINT = 75
    // // , FORMAT_R16_SFLOAT = 76
    // // , FORMAT_R16G16_UNORM = 77
    // // , FORMAT_R16G16_SNORM = 78
    // // , FORMAT_R16G16_USCALED = 79
    // // , FORMAT_R16G16_SSCALED = 80
    // // , FORMAT_R16G16_UINT = 81
    // // , FORMAT_R16G16_SINT = 82
    // // , FORMAT_R16G16_SFLOAT = 83
    // // , FORMAT_R16G16B16_UNORM = 84
    // // , FORMAT_R16G16B16_SNORM = 85
    // // , FORMAT_R16G16B16_USCALED = 86
    // // , FORMAT_R16G16B16_SSCALED = 87
    // // , FORMAT_R16G16B16_UINT = 88
    // // , FORMAT_R16G16B16_SINT = 89
    // // , FORMAT_R16G16B16_SFLOAT = 90
    // // , FORMAT_R16G16B16A16_UNORM = 91
    // // , FORMAT_R16G16B16A16_SNORM = 92
    // // , FORMAT_R16G16B16A16_USCALED = 93
    // // , FORMAT_R16G16B16A16_SSCALED = 94
    // // , FORMAT_R16G16B16A16_UINT = 95
    // // , FORMAT_R16G16B16A16_SINT = 96
    // // , FORMAT_R16G16B16A16_SFLOAT = 97
    // // , FORMAT_R32_UINT = 98
    // // , FORMAT_R32_SINT = 99
    // // , FORMAT_R32_SFLOAT = 100
    // // , FORMAT_R32G32_UINT = 101
    // // , FORMAT_R32G32_SINT = 102
    // R32G32_SFLOAT,
    // // , FORMAT_R32G32B32_UINT = 104
    // // , FORMAT_R32G32B32_SINT = 105
    // R32G32B32_SFLOAT,
    // // , FORMAT_R32G32B32A32_UINT = 107
    // // , FORMAT_R32G32B32A32_SINT = 108
    // // , FORMAT_R32G32B32A32_SFLOAT = 109
    // // , FORMAT_R64_UINT = 110
    // // , FORMAT_R64_SINT = 111
    // // , FORMAT_R64_SFLOAT = 112
    // // , FORMAT_R64G64_UINT = 113
    // // , FORMAT_R64G64_SINT = 114
    // // , FORMAT_R64G64_SFLOAT = 115
    // // , FORMAT_R64G64B64_UINT = 116
    // // , FORMAT_R64G64B64_SINT = 117
    // // , FORMAT_R64G64B64_SFLOAT = 118
    // // , FORMAT_R64G64B64A64_UINT = 119
    // // , FORMAT_R64G64B64A64_SINT = 120
    // // , FORMAT_R64G64B64A64_SFLOAT = 121
    // // , FORMAT_B10G11R11_UFLOAT_PACK32 = 122
    // // , FORMAT_E5B9G9R9_UFLOAT_PACK32 = 123
    // // , FORMAT_D16_UNORM = 124
    // // , FORMAT_X8_D24_UNORM_PACK32 = 125
    // // , FORMAT_D32_SFLOAT = 126
    // // , FORMAT_S8_UINT = 127
    // // , FORMAT_D16_UNORM_S8_UINT = 128
    // // , FORMAT_D24_UNORM_S8_UINT = 129
    // // , FORMAT_D32_SFLOAT_S8_UINT = 130
    // // , FORMAT_BC1_RGB_UNORM_BLOCK = 131
    // // , FORMAT_BC1_RGB_SRGB_BLOCK = 132
    // // , FORMAT_BC1_RGBA_UNORM_BLOCK = 133
    // // , FORMAT_BC1_RGBA_SRGB_BLOCK = 134
    // // , FORMAT_BC2_UNORM_BLOCK = 135
    // // , FORMAT_BC2_SRGB_BLOCK = 136
    // // , FORMAT_BC3_UNORM_BLOCK = 137
    // // , FORMAT_BC3_SRGB_BLOCK = 138
    // // , FORMAT_BC4_UNORM_BLOCK = 139
    // // , FORMAT_BC4_SNORM_BLOCK = 140
    // // , FORMAT_BC5_UNORM_BLOCK = 141
    // // , FORMAT_BC5_SNORM_BLOCK = 142
    // // , FORMAT_BC6H_UFLOAT_BLOCK = 143
    // // , FORMAT_BC6H_SFLOAT_BLOCK = 144
    // // , FORMAT_BC7_UNORM_BLOCK = 145
    // // , FORMAT_BC7_SRGB_BLOCK = 146
    // // , FORMAT_ETC2_R8G8B8_UNORM_BLOCK = 147
    // // , FORMAT_ETC2_R8G8B8_SRGB_BLOCK = 148
    // // , FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK = 149
    // // , FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK = 150
    // // , FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK = 151
    // // , FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK = 152
    // // , FORMAT_EAC_R11_UNORM_BLOCK = 153
    // // , FORMAT_EAC_R11_SNORM_BLOCK = 154
    // // , FORMAT_EAC_R11G11_UNORM_BLOCK = 155
    // // , FORMAT_EAC_R11G11_SNORM_BLOCK = 156
// }
}
