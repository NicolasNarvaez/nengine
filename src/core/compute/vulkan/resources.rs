
use std::marker::PhantomData;
use std::os::raw::c_void;

use ash::{vk::Format, vk};

use crate::core::compute::resources::buffers::{VertexInputDescription, VertexBuffer};
use crate::core::compute::resources::scene::RenderTypeT;
use crate::core::compute::resources::{Image, Extent, FormatTypeVendorMap};

use crate::core::compute::{resources::{Format as CoreFormat, format}, vulkan::ComputeDevice};

use super::ComputeResult;
// use super::{super::resources::{Format as CoreFormat, format}, ComputeDevice};

pub mod util {
    pub fn power_of_2(values: &[usize]) -> Vec<usize> {
	values.iter()
	    .map(|&val| {
		let mut exponent = 0;
		if val & val-1 != 0 {return 0}

		let mut num = val;
		while num > 1 {
		    num >>= 1;
		    exponent += 1;
		}

		exponent as usize
	    })
	    .collect()
    }
}

impl From<CoreFormat> for Format {
    fn from(src: CoreFormat) -> Format {
	match src {
	    CoreFormat::UNDEFINED => Format::UNDEFINED,
	    CoreFormat::R8G8B8A8_SRGB => Format::R8G8B8A8_SRGB,
	    CoreFormat::R32G32_SFLOAT => Format::R32G32_SFLOAT,
	    CoreFormat::R32G32B32_SFLOAT => Format::R32G32B32_SFLOAT,
	    CoreFormat::R32G32B32A32_SFLOAT => Format::R32G32B32A32_SFLOAT,
	    other => panic!("Invalid format conversion: {:?}", other),
	}
    }
}

impl FormatTypeVendorMap<Format> for format::UNDEFINED { 
    fn get_format() -> Format { Format::UNDEFINED } }
impl FormatTypeVendorMap<Format> for format::R8G8B8A8_SRGB { 
    fn get_format() -> Format { Format::R8G8B8A8_SRGB } }
impl FormatTypeVendorMap<Format> for format::R32G32_SFLOAT { 
    fn get_format() -> Format { Format::R32G32_SFLOAT } }
impl FormatTypeVendorMap<Format> for format::R32G32B32_SFLOAT { 
    fn get_format() -> Format { Format::R32G32B32_SFLOAT } }
impl FormatTypeVendorMap<Format> for format::R32G32B32A32_SFLOAT { 
    fn get_format() -> Format { Format::R32G32B32A32_SFLOAT } }

pub struct RenderTypeS<'a> {phantom: PhantomData<&'a ()>}

impl<'a> RenderTypeT for RenderTypeS<'a> {
    type Image = DeviceImage<'a>;
    type Buffer = DeviceBuffer<'a>;
    type ShaderModule = DeviceShaderModule<'a>;
}

pub type RenderType<'a> = crate::core::compute::resources::scene::RenderType<'a, RenderTypeS<'a>>;

pub struct DeviceShaderModule<'a> {
    pub device: &'a ComputeDevice<'a>,
    pub module: vk::ShaderModule,
}

pub struct DeviceBuffer<'a> {
    pub device: &'a ComputeDevice<'a>,
    pub buffer: vk::Buffer,
    pub memory: vk::DeviceMemory,
}

impl<'a> DeviceBuffer<'a> {
    pub fn from_data(device: &'a ComputeDevice<'a>, pool: vk::CommandPool, queue: vk::Queue, 
			      usage: vk::BufferUsageFlags,
			      data: & [u8]) -> ComputeResult<Self> {

	let (buffer, memory) = unsafe {
	    device.create_device_buffer(pool, queue, 
					data.as_ptr() as *const c_void, data.len() as u64, 
					usage) }?;
	Ok(Self {
	    device,
	    buffer,
	    memory,
	})
    }
}

pub struct DeviceImage<'a> {
    pub device: &'a ComputeDevice<'a>,
    pub format: Format,

    pub image: vk::Image,
    pub memory: vk::DeviceMemory,
    pub image_view: vk::ImageView,
    pub sampler: vk::Sampler,
}

impl<'a> DeviceImage<'a> {
    pub fn new<'b>(device: &'a ComputeDevice<'a>, pool: vk::CommandPool, queue: vk::Queue, 
			 host_image: &'b Image<DeviceImage>) -> DeviceImage<'a> 
    {
	
	let format = host_image.format.clone().into();

	println!("Creating a device image!!");
	let (image, memory, image_view, sampler) 
	= DeviceImage::load(device, pool, queue, &host_image.extent, format, &host_image.data).unwrap();

        Self { device, format, 
	    image, memory, image_view, sampler }
    }

    pub fn load(device: &'a ComputeDevice<'a>, pool: vk::CommandPool, queue: vk::Queue,
		      src_extent: & Extent, format: vk::Format, data: & Box<[u8]>) 
    -> Result<(vk::Image, vk::DeviceMemory, vk::ImageView, vk::Sampler), String>  
    {
	let dims = src_extent.len();

	// compute device extent
	let mut extent = vk::Extent3D {depth:1, height:1, width:1};
	let src_extent_exps = util::power_of_2(src_extent);
	
	println!("shape: {:?}", src_extent);
	println!("shape exponents: {:?}", src_extent_exps);

	if src_extent_exps.iter().find(|&&val| val == 0).is_some() {panic!("Img size not power of 2");}
	if dims > 3 {
	    let extra_exps = src_extent_exps[3..].iter().cloned().reduce(|acc, val| acc + val).unwrap();

	    let dim_remainder = (extra_exps%2) as u32;
	    let square_extension = (extra_exps/2) as u32;

	    
	    extent.depth = (src_extent[0] * usize::pow(2, dim_remainder)) as u32;
	    extent.height = (src_extent[1] * usize::pow(2, square_extension)) as u32;
	    extent.width = (src_extent[2] * usize::pow(2, square_extension)) as u32;

	    println!("Compressed extent extra dims {:?}, extension {:?}, remainder {:?}", 
		     extra_exps, square_extension, dim_remainder);
	}
	else if dims == 3 {
	    extent.depth = src_extent[0] as u32;
	    extent.height = src_extent[1] as u32;
	    extent.width = src_extent[2] as u32;
	}
	else if dims == 2 {
	    extent.height = src_extent[0] as u32;
	    extent.width = src_extent[1] as u32;
	}
	else if dims == 1 {
	    extent.width = src_extent[0] as u32;
	}

	let size = data.len();

	let image_type = if dims == 1 {vk::ImageType::TYPE_1D} 
	    else if dims == 2 {vk::ImageType::TYPE_2D}
	    else {vk::ImageType::TYPE_3D};

	println!("extent: {:?}", extent);
	println!("size: {:?}", size);
	println!("format: {:?}", format);
	println!("image_type: {:?}", image_type);

	let (image, memory, view) 
	    = unsafe {device.load_image(
		    pool, queue, format, size as u64, extent, image_type, data.as_ptr() as *mut c_void)}.unwrap();

	let sampler = unsafe {device.create_texture_sampler_linear()}.unwrap();

	Ok((image, memory, view, sampler))
    }
	// void load() {
		// if(image != VK_NULL_HANDLE) return;
		// device->loadImage(device->graphics_queue
				// , format, host_image->data, (VkDeviceSize)host_image->size_bytes, host_image->width, host_image->height
				// , image, memory, view);
		// ComputeDevice::createTextureSamplerLinear(device, sampler);
	// };
	// void unload() {
		// vkDestroySampler(device->logical_device, sampler, nullptr);
		// vkDestroyImageView(device->logical_device, view, nullptr);
		// vkDestroyImage(device->logical_device, image, nullptr);
		// vkFreeMemory(device->logical_device, memory, nullptr);
	// };
}
