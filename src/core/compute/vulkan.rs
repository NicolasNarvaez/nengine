use crate::core::{wraps, os::OSWindow};
use std::{ffi::CStr, collections::{HashMap, HashSet}, sync::{Arc, RwLock}, os::raw::c_void};

use ash::{vk, prelude::VkResult, extensions::{khr, ext::DebugUtils}, Entry};
use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};

use self::resources::{DeviceImage, RenderType};

use super::resources::buffers::{VertexInputDescription, HostBufferPtr};

pub mod resources;

#[derive(Debug, Eq, PartialEq)]
pub enum ComputeError {
	NoMemoryType,
	NoSuportedFormat,
	UnsupportedLayoutTransition,
	VkError(ash::vk::Result),
}

impl From<vk::Result> for ComputeError {
	fn from(result: vk::Result) -> Self {ComputeError::VkError(result)}
}


// TODO: investigate more idiomatic error handling (into/from + ?), newtype currently breaks ?
// use derive_more::{Deref, DerefMut};
// #[derive(Deref, DerefMut)]
// pub struct ComputeResult<T> (Result<T, ComputeError>);
// impl<T> From<Result<T, vk::Result>> for ComputeResult<T> {
    // fn from(value: Result<T, vk::Result>) -> Self {
		// ComputeResult(match value {
			// Ok(v) => Ok(v),
			// Err(e) => Err(ComputeError::VkError(e)),
		// })
    // }
// }

pub type ComputeResult<T> = Result<T, ComputeError>;

// orphan rule my f**g balls (we need more (s)coping semantics)
pub fn result_from_vk<T>(result: Result<T, vk::Result>) -> ComputeResult<T> {
	match result {
		Ok(v) => Ok(v),
		Err(e) => Err(ComputeError::VkError(e)),
	}
}
pub fn vk_into_result<T>(vk_result: vk::Result, val: Option<T>) -> VkResult<T> {
	match vk_result {
		vk::Result::SUCCESS => Ok(val.unwrap()),
		err => Err(err),
	}
}

#[derive(Debug)]
pub struct QueueFamily {
	pub index: u32,
	pub queues: u32,
	pub props: vk::QueueFamilyProperties,
}

pub struct QueueFamilyFeatures<'a> {
	pub queue_types: Vec<QueueType>,
	pub param_surface: Option<&'a vk::SurfaceKHR>,
}

#[derive(Debug)]
pub struct Queue<'a> {
	pub family: &'a QueueFamily,
	pub handle: vk::Queue,
}

// Based on Vulkan
#[derive(Copy, Clone)]
pub enum QueueType {
	Graphics,
	Compute,
	Transfer,
	SparseBinding,
	Protected,
	VideoDecode,
	VideoEncode,
	VkOpticalFlow,
}

impl Into<vk::QueueFlags> for QueueType {
	fn into(self) -> vk::QueueFlags {
		use vk::QueueFlags;

		match self {
			QueueType::Graphics => QueueFlags::GRAPHICS,
			QueueType::Compute => QueueFlags::COMPUTE,
			QueueType::Transfer => QueueFlags::TRANSFER,
			QueueType::SparseBinding => QueueFlags::SPARSE_BINDING,
			QueueType::Protected => QueueFlags::PROTECTED,
			QueueType::VideoDecode => QueueFlags::VIDEO_DECODE_KHR,
			QueueType::VideoEncode => QueueFlags::VIDEO_ENCODE_KHR,
			QueueType::VkOpticalFlow => QueueFlags::OPTICAL_FLOW_NV,
		}
	}
}

// impl Into<vk::QueueFlags> for &QueueType {
	// fn into(self) -> vk::QueueFlags {
		// let copy = *self.clone();
		// copy.into()
	// }
// }

type QueueDict = std::collections::HashMap<String, usize>;

pub struct PhysicalDevice {
	pub index: u32,
	pub handle: vk::PhysicalDevice,

	pub extensions: Vec<vk::ExtensionProperties>,
	pub properties: vk::PhysicalDeviceProperties,
	pub features: vk::PhysicalDeviceFeatures,
	pub mem_props: vk::PhysicalDeviceMemoryProperties,

	pub queue_families: Vec<QueueFamily>,
	// pub queue_families_map_indices: std::collections::HashMap<String, u32>,
}

// lists fn ptr tables to load
pub struct APIFeatures<'a> {
	pub extension_ext_debug_utils: bool,
	pub ext_khr_surface: bool,
	pub layer_validation: bool,
	pub param_window_builder: Option<&'a winit::window::Window>,
}

impl<'a> APIFeatures<'a> {
	// pub fn add_window_builder() {}
}

impl<'a> Default for APIFeatures<'a> {
	fn default() -> APIFeatures<'a> {
		APIFeatures { 
			extension_ext_debug_utils: true,
			ext_khr_surface: true, 
			layer_validation: true,
			param_window_builder: None,
		}
	}
}

// Holds all function pointer tables
// update must be linked with mutator of state (settings/capabilities subsystem)
pub struct APIInstance {
	pub entry: ash::Entry,
	pub instance: ash::Instance,
	pub ext_khr_surface: Option<khr::Surface>,

	pub physical_devices: Vec<PhysicalDevice>,
}

// vulkan::{ ComputeContext , devices}
// graphic_features -> layers, extensions
impl APIInstance {

	pub fn new(app_name: &String, engine_name: &String, api_features: &APIFeatures,) -> Self {
		let entry = Entry::linked();

		let mut layer_names : Vec<*const i8> = Vec::new();
		let mut extension_names : Vec<*const i8> = Vec::new();

		if api_features.layer_validation {
			layer_names.push(unsafe {CStr::from_ptr("VK_LAYER_KHRONOS_validation\0".as_ptr() as * const i8).as_ptr() as *const i8 })
		}

		if api_features.extension_ext_debug_utils {
			extension_names.push(DebugUtils::name().as_ptr());
		}
		if let Some(window) = api_features.param_window_builder {
			extension_names.extend(
				ash_window::enumerate_required_extensions(window.raw_display_handle())
				.expect("Cant get window extensions").to_vec()
				)
		}

		let extension_names_cstr = extension_names.iter().map(|ext| {
			unsafe { CStr::from_ptr(*ext) }
		}).collect::<Vec<&CStr>>();
		println!("Instance Extensions: {:?}", extension_names_cstr);

		let app_info = vk::ApplicationInfo {
			p_application_name : app_name.as_str().as_ptr() as * const i8,
			application_version : 0,
			p_engine_name : engine_name.as_str().as_ptr() as * const i8,
			engine_version : 0,
			api_version : vk::make_api_version(0, 1, 0, 0),
			..Default::default()
		};

		let create_flags = if cfg!(any(target_os = "macos", target_os = "ios"))
			{ vk::InstanceCreateFlags::ENUMERATE_PORTABILITY_KHR }
		else { vk::InstanceCreateFlags::default() };

		let create_info = vk::InstanceCreateInfo {
			p_application_info : &app_info,
			enabled_layer_count : layer_names.len() as u32,
			pp_enabled_layer_names : layer_names.as_ptr(),
			enabled_extension_count : extension_names.len() as u32,
			pp_enabled_extension_names : extension_names.as_ptr(),
			flags : create_flags,
			..Default::default()
		};
		let instance = unsafe{ entry.create_instance(&create_info, None).expect("instance creation error") };

		let ext_khr_surface = 
			if api_features.ext_khr_surface { Some(khr::Surface::new(&entry, &instance)) } 
			else {None};

		let physical_devices = unsafe { instance.enumerate_physical_devices() }
			.expect("Cant enumerate physical devices")
			.iter().enumerate().map(|(device_index, &physical_device)| {

				let queue_families = unsafe{instance.get_physical_device_queue_family_properties(physical_device)}
				.iter().enumerate().map(|(index, props)| QueueFamily { 
							index: index as u32, 
							queues: 1,
							props: props.clone() }).collect();

				let available_extensions = unsafe {
						instance.enumerate_device_extension_properties(physical_device) 
					}.expect("Cant enumerate device extensions props");
				let available_physical_features = unsafe { instance.get_physical_device_features(physical_device) };
				let physical_mem_props = unsafe{instance.get_physical_device_memory_properties(physical_device)};
				let physical_properties = unsafe{instance.get_physical_device_properties(physical_device)};

				PhysicalDevice {
					  index: device_index as u32,
					  handle: physical_device,
					  features: available_physical_features,
					  properties: physical_properties,
					  mem_props: physical_mem_props,
					  extensions: available_extensions,
					  queue_families,
				  }
			}).collect();

		APIInstance {entry, instance, ext_khr_surface, physical_devices}
	}

	// Selects device preloaded on APIInstance compatible with DeviceFeatures
	pub fn select_physical_device(& self, device_features: & DeviceFeatures) 
		-> Option<(&PhysicalDevice, DeviceRequiredExtensions, vk::PhysicalDeviceFeatures, QueueDict)>	{

		let APIInstance {instance, physical_devices, ..} = self;
		let ext_khr_surface = self.ext_khr_surface.as_ref();

		// if ext_khr_surface.is_none() && device_features.queue_present
			// { panic!("Instance extension not loaded, but present queue requested! (SURFACE_KHR)") }

		let mut required_extensions = vec![
			#[cfg(any(target_os = "macos", target_os = "ios"))]
			KhrPortabilitySubsetFn::NAME.as_ptr(),
		];

		if device_features.extension_khr_swapchain { required_extensions.push(khr::Swapchain::name().as_ptr()) }

		let required_device_features = vk::PhysicalDeviceFeatures {
			shader_clip_distance: device_features.feature_shader_clip_distance,
			sampler_anisotropy: device_features.feature_sampler_anisotropy,
			..Default::default()
		};

		// let physical_devices = unsafe { instance.enumerate_physical_devices() }
			// .expect("Cant enumerate physical devices");

		println!("Physical Devices N°: {:?}", physical_devices.len());

		let device_extension_names_strings : Vec<String> = required_extensions.iter().map( 
			|ptr| unsafe { String::from(CStr::from_ptr(*ptr).to_str().unwrap()) } ).collect();

		println!("Required Extensions: {:?}", device_extension_names_strings);
		println!("Required Features: {:#?}", required_device_features);

		let selected = physical_devices.iter()
			.enumerate().find_map(|(device_index, physical_device)| {

				let PhysicalDevice {
					extensions: available_extensions, 
					features: available_physical_features, 
					..
				} = physical_device;

				let mut supports_queues = device_features.queue_families.is_some() == false;
				let supports_extensions: bool;

				/////////// QUEUES
				let mut families_map = HashMap::new();
				
				if !supports_queues {
					let features = device_features.queue_families.as_ref().unwrap();

					println!("checking device props");
					for(name, QueueFamilyFeatures { queue_types, param_surface }) in features.iter() {
						for (queue_index, family) in physical_device.queue_families.iter().enumerate() {

								// let QueueFamilyFeatures { queue_types, param_surface } = family_features;

								let supports_surface = param_surface.is_none() 
									|| unsafe { 
										ext_khr_surface?.get_physical_device_surface_support
											(physical_device.handle, queue_index as u32, *param_surface.unwrap()) 
									}.unwrap();

								let supports_type = queue_types.iter().filter(
									|&queue_type| !family.props.queue_flags.contains((*queue_type).into())).count() == 0;

								if supports_surface && supports_type {families_map.insert(name.clone(), queue_index); break;}
						}
					}

					supports_queues = families_map.len() == features.len();

					if !supports_queues {return None;}
				}

				/////////// EXTENSIONS
				let unsupported_extensions : Vec<_> = required_extensions.iter().filter(
					|&ext| {!(available_extensions.iter().any(
								|available_extension| {
									unsafe {
									CStr::from_ptr(available_extension.extension_name.as_ptr())
										.eq(CStr::from_ptr(*ext))
									}
								}
								))}
					).collect();

				supports_extensions = unsupported_extensions.len() == 0;

				if !supports_extensions {
					println!("Unsupported extensions (device_index:{device_index}): ");
					for ext in unsupported_extensions { println!("	| {:?}", unsafe { CStr::from_ptr(*ext) }); }
				}

				/////////// FEATURES

				let supports_features = wraps::ash::vk::PhysicalDeviceFeatures::le(
					&required_device_features, &available_physical_features);
				if !supports_extensions { println!("Unsupported features!!"); }

				/////////// Create
				if !(supports_queues && supports_extensions && supports_features) {return Option::None;}

				Option::Some((physical_device, families_map))
			})?;

			let (physical_device, .., families_map) = &selected;

			

			////// Print Device Properties
			println!("Found suitable device:");
			println!("Required Extensions: {:?}", device_extension_names_strings);
			APIInstance::print_extensions("Available Extensions: ", &physical_device.extensions);
			println!("Required Features: {:#?}", required_device_features);
			println!("Supported Features: {:#?}", physical_device.features);
			println!("Physical Devices N°: {:?}", physical_devices.len());
			println!("device_index: {}, families_map: {:?}",
					 physical_device.index,
					 families_map);

			Some((physical_device, required_extensions, required_device_features, selected.1))
	}

	fn print_extensions(msg: &str, extensions: &Vec<ash::vk::ExtensionProperties>) {
		let extensions : Vec<_> = extensions.iter().map(
			|extension| unsafe {CStr::from_ptr(extension.extension_name.as_ptr())}).collect();

		println!("{:?}", msg);
		for ext in extensions {
			println!("	| {:?}", ext);
		}
	}


	pub fn create_surface(&self, window: winit::window::Window) -> vk::SurfaceKHR {
		let surface = unsafe { ash_window::create_surface(
			&self.entry,
			&self.instance,
			window.raw_display_handle(),
			window.raw_window_handle(),
			None,
			)
			.expect("Cant create window surface!")
		};

		println!("created surface");

		surface
	}
}

pub struct CommandPool<'a> {
	pub family: &'a QueueFamily,
	pub handle: vk::CommandPool,
}

impl<'a> PartialEq for CommandPool<'a> {
	fn eq(&self, rhs: &Self) -> bool { self.handle == rhs.handle }
}
impl<'a> Eq for CommandPool<'a> {}

impl<'a> std::hash::Hash for CommandPool<'a> {
	fn hash<H: std::hash::Hasher>(&self, hasher: &mut H) { self.handle.hash(hasher) }
}

pub struct DeviceFeatures<'a> {
	pub extension_khr_swapchain: bool,
	pub feature_shader_clip_distance: u32,
	pub feature_sampler_anisotropy: u32,
	// pub param_surface: Option<&'a vk::SurfaceKHR>,
	pub param_surface: Option<&'a vk::SurfaceKHR>,

	// device queue usage TODO: replace by queue family graph
	pub queue_present: bool,
	pub queue_graphics: bool,
	pub queue_compute: bool,

	pub queue_families: Option<HashMap<String, QueueFamilyFeatures<'a>>>,
}

impl<'a> DeviceFeatures<'a> {

	// generates non-borrowing copy for record purposes
	pub fn as_record<'b>(&self) -> DeviceFeatures<'b> {
		DeviceFeatures {
			param_surface: None,
			queue_families: None, // TODO map to something
			..*self
		}
	}

	pub fn base_presentation(surface: &'a vk::SurfaceKHR) -> Self {
		DeviceFeatures {
			queue_families: Some([
				(String::from("graphics"), QueueFamilyFeatures { 
					queue_types: vec![QueueType::Graphics], 
					param_surface: None,}), 
				(String::from("present"), QueueFamilyFeatures { 
					queue_types: vec![], 
					param_surface: Some(surface),}), 
			].into_iter().collect()),
			..Default::default()
		}
	}
}

impl<'a> Default for DeviceFeatures<'a> {
	fn default() -> DeviceFeatures<'a> {
		DeviceFeatures {
			extension_khr_swapchain: true,
			feature_shader_clip_distance: 1,
			feature_sampler_anisotropy: 1,
			param_surface: None,

			// TODO: replace by queuegraph => define queues & merging priorities => instanced queuegraph + queues
			// currently only base default behaviour is impl:
			//		present and graphics will be in same queuefamily if possible
			//		compute will be always separate
			queue_present: true,
			queue_graphics: true,
			queue_compute: true,

			queue_families: Some([
				(String::from("graphics"), QueueFamilyFeatures { 
					queue_types: vec![QueueType::Graphics], 
					param_surface: None,}), 
			].into_iter().collect()),
			
		}
	}
}

pub type DeviceRequiredExtensions = Vec<* const i8>;

pub struct ComputeDevice<'a> {
	pub api_instance: &'a APIInstance,
	pub instance: &'a ash::Instance, 

	pub physical_device: &'a PhysicalDevice, // must point to instance TODO
	pub queue_families_map: HashMap<String, &'a QueueFamily>,

	pub fns: ash::Device,
	pub ext_khr_swapchain: Option<khr::Swapchain>,

	pub queues: HashMap<String, Vec<Queue<'a>>>,

	pub command_pools: RwLock<HashMap<String, HashSet<Arc<CommandPool<'a>>> >>,
}

impl<'a> ComputeDevice<'a> {

	// All queues for all requested families are retrieved from device.
	pub fn new(api_instance: &'a APIInstance, device_features: & DeviceFeatures) -> Self { 

		let APIInstance{instance , ..} = api_instance;

		let (physical_device, required_extensions, required_features, queue_families_map) = api_instance.select_physical_device(device_features).unwrap();

		let PhysicalDevice {queue_families, ..} = physical_device;

		///////////////////////////////////////////////// Create Logical Device
		println!("Queue Families N°: {}", queue_families.len());

		// if they merged, create two queues in same family
		// let queue_count = if queue_families.len() == 1 && queue_list.len() == 2 {2} else {1};
		let priorities = 1.0;

		let queue_create_infos = queue_families.iter().map(|queue_family| vk::DeviceQueueCreateInfo {
			queue_family_index: queue_family.index,
			// queue_count, // take max queues into account on queue_graph
			queue_count: 1,
			// p_queue_priorities: priorities.as_
			p_queue_priorities: std::slice::from_ref(&priorities).as_ptr(),
			..Default::default()
		}).collect::<Vec<_>>();

		let device_create_info = vk::DeviceCreateInfo {
			queue_create_info_count: queue_create_infos.len() as u32,
			p_queue_create_infos: queue_create_infos.as_slice().as_ptr(),
			enabled_extension_count: required_extensions.len() as u32,
			pp_enabled_extension_names: required_extensions.as_ptr(),
			p_enabled_features: &required_features,
			..Default::default()};

		let fns = unsafe { instance.create_device(physical_device.handle, &device_create_info, None) }
			.expect("Cant create logical device");

		let ext_khr_swapchain = if device_features.extension_khr_swapchain 
				{ Some(khr::Swapchain::new(&instance, &fns))}
			else { None };

		let queue_families_map : HashMap<_,_> = queue_families_map.iter()
			.map(|(name, index)| (name.clone(), physical_device.queue_families.get(*index).unwrap())).collect();

		let queues = queue_families_map.iter().map(|(family_name, family)|	(
					family_name.clone(),
					(0..family.queues).map(|queue_index| Queue {
						handle: unsafe {fns.get_device_queue(family.index, queue_index)},
						family, }).collect())).collect();

		let command_pools = RwLock::new(queue_families_map.iter()
			.map(|(family_name,_)| (family_name.clone(), HashSet::new()))
			.collect());

		println!("QueuefamiliesMap: {:?}", queue_families_map );
		println!("Queuefamilies: {:?}", queues );

		let compute_device = ComputeDevice { 
			api_instance,
			instance,
			physical_device,
			fns,
			queue_families_map,
			ext_khr_swapchain,

			command_pools,
			queues,
		};

		compute_device

	}

	pub fn create_image_view(&self, image: vk::Image, format: vk::Format, aspect_flags: vk::ImageAspectFlags) 
		-> VkResult<vk::ImageView> {

		let create_info = vk::ImageViewCreateInfo {
			image,
			view_type: vk::ImageViewType::TYPE_2D,
			format,
			subresource_range: vk::ImageSubresourceRange {
				aspect_mask: aspect_flags,
				base_mip_level: 0,
				level_count: 1,
				base_array_layer: 0,
				layer_count: 1,
				..Default::default()
			},
			..Default::default()
		};

		unsafe { self.fns.create_image_view(&create_info, None) }
	}

	pub unsafe fn create_shader_module(&self, code: &[u32] ) -> ComputeResult<vk::ShaderModule> {
		let create_info = vk::ShaderModuleCreateInfo::builder().code(code).build();

		Ok(self.fns.create_shader_module(&create_info, None)?)
	}
	

	// TODO queue index == command_pool.family_index => create queue struct, add as command_pool reqs
	// Submits/awaits image transition correlating access masks and stages, sets aspectMask according to format.
	pub fn transition_image_layout(&self, command_pool: vk::CommandPool, queue: vk::Queue, 
								   image: vk::Image, format: vk::Format, 
								   old_layout: vk::ImageLayout, new_layout: vk::ImageLayout
								   ) -> ComputeResult<()> 
	{
		let command_buffer = self.begin_single_time_cmds(command_pool)?;

		let src_access_mask;
		let dst_access_mask;

		let src_stage;
		let dst_stage;

		if old_layout == vk::ImageLayout::UNDEFINED
			&& new_layout == vk::ImageLayout::TRANSFER_DST_OPTIMAL {

				src_access_mask = vk::AccessFlags::NONE;
				dst_access_mask = vk::AccessFlags::TRANSFER_WRITE;

				src_stage = vk::PipelineStageFlags::TOP_OF_PIPE;
				dst_stage = vk::PipelineStageFlags::TRANSFER;
			}
		else if old_layout == vk::ImageLayout::TRANSFER_DST_OPTIMAL
			&& new_layout == vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL {

				src_access_mask = vk::AccessFlags::TRANSFER_WRITE;
				dst_access_mask = vk::AccessFlags::SHADER_READ;

				src_stage = vk::PipelineStageFlags::TRANSFER;
				dst_stage = vk::PipelineStageFlags::FRAGMENT_SHADER;
			}
		else if old_layout == vk::ImageLayout::UNDEFINED
			&& new_layout == vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL {

				src_access_mask = vk::AccessFlags::NONE;
				dst_access_mask = vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ 
					| vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE;

				src_stage = vk::PipelineStageFlags::TOP_OF_PIPE;
				dst_stage = vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS;
			}
		else {
			return Err(ComputeError::UnsupportedLayoutTransition)
		}

		let mut barrier = vk::ImageMemoryBarrier {
			src_access_mask, dst_access_mask, old_layout, new_layout,
			src_queue_family_index: vk::QUEUE_FAMILY_IGNORED, dst_queue_family_index: vk::QUEUE_FAMILY_IGNORED,
			image,
			subresource_range: vk::ImageSubresourceRange {
				aspect_mask: vk::ImageAspectFlags::COLOR,
				base_mip_level: 0,
				level_count: 1,
				base_array_layer: 0,
				layer_count: 1,
				..Default::default()
			},
			..Default::default()
		};

		if new_layout == vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
			barrier.subresource_range.aspect_mask = vk::ImageAspectFlags::DEPTH;

			if format == vk::Format::D32_SFLOAT_S8_UINT || format == vk::Format::D24_UNORM_S8_UINT { 
				barrier.subresource_range.aspect_mask |= vk::ImageAspectFlags::STENCIL;
			}
		}

		unsafe { self.fns.cmd_pipeline_barrier(command_buffer, 
						       src_stage, dst_stage, 
						       vk::DependencyFlags::empty(), &[], &[], [barrier].as_ref()) };

		self.end_single_time_cmds(command_pool, command_buffer, queue)?;

		Ok(())
	}

	pub fn find_memory_type(&self, props: &vk::MemoryPropertyFlags, type_filter: u32) -> ComputeResult<u32> {

		for (index, mem_type) in self.physical_device.mem_props.memory_types.iter().enumerate() {
			if ((type_filter & (1 << index)) != 0)
				&& (mem_type.property_flags & *props == *props)
			{ return Ok(index as u32) }
		}

		Err(ComputeError::NoMemoryType)
	}

	pub fn find_supported_format(&self, candidates: &Vec<vk::Format>
								 , tiling: vk::ImageTiling
								 , features: vk::FormatFeatureFlags) -> ComputeResult<vk::Format>
	{
		for format in candidates.iter() {
			let props = unsafe {
				self.instance.get_physical_device_format_properties(self.physical_device.handle, *format)};

			if tiling == vk::ImageTiling::LINEAR && ((props.linear_tiling_features & features) == features) 
			{ return Ok(*format) }

			if tiling == vk::ImageTiling::OPTIMAL && ((props.optimal_tiling_features & features) == features) 
			{ return Ok(*format) }
		}

		Err(ComputeError::NoSuportedFormat)
	}

	// TODO: add queue indices to compute device, (queue props, indices, & instance ...)

	fn print_extensions(msg: &str, extensions: &Vec<ash::vk::ExtensionProperties>) {
		let extensions : Vec<_> = extensions.iter().map(
			|extension| unsafe {CStr::from_ptr(extension.extension_name.as_ptr())}).collect();

		println!("{:?}", msg);
		for ext in extensions {
			println!("	| {:?}", ext);
		}
	}

	pub fn create_command_pool(&self, queue_family_index: u32) -> vk::CommandPool {

		let create_info = vk::CommandPoolCreateInfo {
			flags: vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
			queue_family_index,
			..Default::default()
		};

		let handle = unsafe { self.fns.create_command_pool(&create_info, None) }
			.expect("Cant create command pool");

		handle
	}

	pub fn find_family_name(&self, queue_family_index: u32) -> Option<&String> {
		self.queue_families_map.iter().find(|(_, val)| val.index == queue_family_index).map(|(key, _)| key)
	}

	pub fn get_queue(&self, family_name: &String, index: Option<usize>) -> Option<&Queue> {
		self.queues.get(family_name).unwrap().get(index.unwrap_or(0))
	}

	pub fn add_command_pool(&self, family_name: &String) -> Arc<CommandPool> {
		let family_index = self.queue_families_map.get(family_name).unwrap().index;
		let handle = self.create_command_pool(family_index);

		let mut set_lock = self.command_pools.write().unwrap();
		let set = set_lock.get_mut(family_name).unwrap();

		let pool = Arc::new(CommandPool { 
			handle, 
			family: self.physical_device.queue_families.get(family_index as usize).unwrap(),
		});
		set.insert(Arc::clone(&pool));

		pool
	}

	pub fn allocate_command_buffers(&self, pool: vk::CommandPool, amount: u32) -> VkResult<Vec<vk::CommandBuffer>> {
		let create_info = vk::CommandBufferAllocateInfo {
			command_pool: pool,
			level: vk::CommandBufferLevel::PRIMARY,
			command_buffer_count: amount,
			..Default::default()
		};

		unsafe{self.fns.allocate_command_buffers(&create_info)}
	}

	// Creates command buffer (PRIMARY), begins recording (ONE_TIME_SUBMIT)
	pub fn begin_single_time_cmds(&self, pool: vk::CommandPool) -> VkResult<vk::CommandBuffer> {
		let create_info = vk::CommandBufferAllocateInfo {
			command_pool: pool,
			level: vk::CommandBufferLevel::PRIMARY,
			command_buffer_count: 1,
			..Default::default()
		};

		let command_buffer = unsafe {self.fns.allocate_command_buffers(&create_info)?[0]};

		let begin_info = vk::CommandBufferBeginInfo {
			flags: vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
			..Default::default()
		};

		unsafe {self.fns.begin_command_buffer(command_buffer, &begin_info)?}

		Ok(command_buffer)
	}

	// Stops recording, submits to execution, vkQueueWaitIdle(), then frees the command buffer
	pub fn end_single_time_cmds(&self, pool: vk::CommandPool, command_buffer: vk::CommandBuffer, queue: vk::Queue) 
	-> VkResult<()> {
		unsafe {self.fns.end_command_buffer(command_buffer)}?;

		let submit_info = vk::SubmitInfo {
			command_buffer_count: 1,
			p_command_buffers: [command_buffer].as_ptr(),
			..Default::default()
		};

		unsafe {self.fns.queue_submit(queue, [submit_info].as_ref(), vk::Fence::null() )}?;
		
		// TODO: use fences
		unsafe {self.fns.queue_wait_idle(queue)}?;

		unsafe {self.fns.free_command_buffers(pool, [command_buffer].as_ref())};

		Ok(())
	}

	// Creates buffer with binded memory.
	pub unsafe fn create_buffer(&self,
					size: vk::DeviceSize, usage: vk::BufferUsageFlags, sharing_mode: vk::SharingMode,
					memory_props: vk::MemoryPropertyFlags)
	-> VkResult<(vk::Buffer, vk::DeviceMemory)> {

		let buffer_info = vk::BufferCreateInfo { size, usage, sharing_mode, ..Default::default() };

		let buffer = self.fns.create_buffer(&buffer_info, None)?;

		let memory_reqs = self.fns.get_buffer_memory_requirements(buffer);

		let alloc_info = vk::MemoryAllocateInfo {
			allocation_size: memory_reqs.size,
			memory_type_index: self.find_memory_type(&memory_props, memory_reqs.memory_type_bits).unwrap(),
			..Default::default()
		};
		let memory = self.fns.allocate_memory(&alloc_info, None)?;

		self.fns.bind_buffer_memory(buffer, memory, 0)?;

		Ok((buffer, memory))
	}

	pub unsafe fn print_buffer(&self,
				   buffer: vk::Buffer,
				   size: vk::DeviceSize) {
		
	}

	pub unsafe fn create_image(&self,
					extent: &vk::Extent3D, image_type: vk::ImageType, format: vk::Format, tiling: vk::ImageTiling,
					usage: vk::ImageUsageFlags, sharing_mode: vk::SharingMode, memory_props: vk::MemoryPropertyFlags) 
	-> ComputeResult<(vk::Image, vk::DeviceMemory)> {

		let image_info = vk::ImageCreateInfo {
			image_type,
			format,
			extent: *extent,
			mip_levels: 1,
			array_layers: 1,
			samples: vk::SampleCountFlags::TYPE_1,
			tiling, usage, sharing_mode, initial_layout: vk::ImageLayout::UNDEFINED,
			..Default::default()
		};

		println!("ImageCreateInfo: {:?}", image_info);
		let image = self.fns.create_image(&image_info, None)?;
		println!("Created image!!");

		let mem_reqs = self.fns.get_image_memory_requirements(image);
		let alloc_info = vk::MemoryAllocateInfo {
			allocation_size: mem_reqs.size,
			memory_type_index: self.find_memory_type(&memory_props, mem_reqs.memory_type_bits)?,
			..Default::default()
		};

		let memory = self.fns.allocate_memory(&alloc_info, None)?;

		self.fns.bind_image_memory(image, memory, 0)?;

		Ok((image, memory))
	}

	pub unsafe fn load_image(&self,
							 pool: vk::CommandPool, queue: vk::Queue,
							 format: vk::Format, size: vk::DeviceSize, extent: vk::Extent3D, image_type: vk::ImageType,
							 data: *const c_void)
	 -> ComputeResult<(vk::Image, vk::DeviceMemory, vk::ImageView)> {

		 let (buffer, buffer_memory
			  ) = self.create_buffer(size, vk::BufferUsageFlags::TRANSFER_SRC, vk::SharingMode::EXCLUSIVE, 
									 vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT)?;
		 self.set_device_memory(data, size, 0, buffer_memory)?;

		 let (image, memory) = self.create_image(&extent, image_type, format, 
												 vk::ImageTiling::OPTIMAL, 
												 vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::SAMPLED, 
												 vk::SharingMode::EXCLUSIVE,
												 vk::MemoryPropertyFlags::DEVICE_LOCAL)?;
		 self.transition_image_layout(pool, queue, image, format, 
									  vk::ImageLayout::UNDEFINED
									  , vk::ImageLayout::TRANSFER_DST_OPTIMAL)?;
		 println!("Before copy");
		 self.copy_buffer_to_image(pool, queue, buffer, image, extent)?;
		 println!("After copy");
		 self.transition_image_layout(pool, queue, image, format, 
									  vk::ImageLayout::TRANSFER_DST_OPTIMAL
									  , vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL)?;

		 self.fns.destroy_buffer(buffer, None);
		 self.fns.free_memory(buffer_memory, None);

		 println!("Before image view");
		 let view = self.create_image_view(image, format, vk::ImageAspectFlags::COLOR)?;
		 println!("After image view");

		 Ok((image, memory, view))
	 }

	pub unsafe fn create_texture_sampler_linear(&self) -> VkResult<vk::Sampler> {
		let props = self.physical_device.properties;

		let sampler_info = vk::SamplerCreateInfo {
			mag_filter: vk::Filter::LINEAR,
			min_filter: vk::Filter::LINEAR,
			mipmap_mode: vk::SamplerMipmapMode::LINEAR,
			address_mode_u: vk::SamplerAddressMode::REPEAT,
			address_mode_v: vk::SamplerAddressMode::REPEAT,
			address_mode_w: vk::SamplerAddressMode::REPEAT,

			mip_lod_bias: 0.0,
			anisotropy_enable: vk::TRUE,
			max_anisotropy: props.limits.max_sampler_anisotropy,
			compare_enable: vk::FALSE,
			compare_op: vk::CompareOp::ALWAYS,
			min_lod: 1.0,
			max_lod: 1.0,
			border_color: vk::BorderColor::INT_OPAQUE_BLACK,
			unnormalized_coordinates: vk::FALSE,
			..Default::default()
		};

		let sampler = self.fns.create_sampler(&sampler_info, None)?;

		Ok(sampler)
	}
					

	pub unsafe fn copy_buffer_to_image(&self, pool: vk::CommandPool, queue: vk::Queue,
								buffer: vk::Buffer, image: vk::Image, extent: vk::Extent3D) 
	-> VkResult<()>   {

		let command_buffer = self.begin_single_time_cmds(pool)?;

		let region = vk::BufferImageCopy {
			buffer_offset: 0,
			buffer_row_length: 0,
			buffer_image_height: 0,
			image_subresource: vk::ImageSubresourceLayers {
				aspect_mask: vk::ImageAspectFlags::COLOR,
				mip_level: 0,
				base_array_layer: 0,
				layer_count: 1,
			},
			image_offset: vk::Offset3D {x: 0, y:0, z: 0},
			image_extent: extent,
			..Default::default()
		};

		self.fns.cmd_copy_buffer_to_image(command_buffer, buffer, image, vk::ImageLayout::TRANSFER_DST_OPTIMAL
										  , &[region]);

		self.end_single_time_cmds(pool, command_buffer, queue)?;

		Ok(())
	}

	pub unsafe fn copy_buffer(&self,
							  pool: vk::CommandPool, queue: vk::Queue, 
							  src_buffer: vk::Buffer, dst_buffer: vk::Buffer, size: vk::DeviceSize) 
	-> VkResult<()> {
		let command_buffer = self.begin_single_time_cmds(pool)?;

		let mut region = vk::BufferCopy::default();
		region.size = size;

		self.fns.cmd_copy_buffer(command_buffer, src_buffer, dst_buffer, &[region]);

		self.end_single_time_cmds(pool, command_buffer, queue)?;

		Ok(())
	}

	// Creates device local exclusive usage buffer, transfers using staging buffer
	pub unsafe fn create_device_buffer(&self,
										pool: vk::CommandPool, queue: vk::Queue, 
										src_data: *const c_void, size: vk::DeviceSize, usage: vk::BufferUsageFlags)
	-> VkResult<(vk::Buffer, vk::DeviceMemory)> {
		if size > (usize::MAX as u64) {return Err(vk::Result::ERROR_OUT_OF_HOST_MEMORY);}

		let (staging_buffer, staging_buffer_memory) = 
			self.create_buffer(size, 
							   vk::BufferUsageFlags::TRANSFER_SRC, 
							   vk::SharingMode::EXCLUSIVE,
							   vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT)?;
		
		// use std::ptr::copy ?
		let ptr = self.fns.map_memory(staging_buffer_memory, 0, size, vk::MemoryMapFlags::empty())?;
		let mapped = std::slice::from_raw_parts_mut(ptr as *mut u8, size as usize);
		mapped.copy_from_slice(std::slice::from_raw_parts_mut(src_data as *mut u8, size as usize));
		self.fns.unmap_memory(staging_buffer_memory);

		let (buffer, memory) = self.create_buffer(size, usage | vk::BufferUsageFlags::TRANSFER_DST, 
												  vk::SharingMode::EXCLUSIVE, 
												  vk::MemoryPropertyFlags::DEVICE_LOCAL)?;

		self.copy_buffer(pool, queue, staging_buffer, buffer, size)?;

		self.fns.destroy_buffer(staging_buffer, None);
		self.fns.free_memory(staging_buffer_memory, None);

		Ok((buffer, memory))
	}

	pub unsafe fn set_device_memory(&self, 
									 src_data: *const c_void, size: vk::DeviceSize, offset: vk::DeviceSize,  
									 memory: vk::DeviceMemory) 
	-> VkResult<()> {
		if size > (usize::MAX as u64) {return Err(vk::Result::ERROR_OUT_OF_HOST_MEMORY);}

		let ptr = self.fns.map_memory(memory, offset, size, vk::MemoryMapFlags::empty())?;
		let mapping = std::slice::from_raw_parts_mut(ptr as *mut u8, size as usize);

		mapping.copy_from_slice(std::slice::from_raw_parts(src_data as *const u8, size as usize));

		self.fns.unmap_memory(memory);

		Ok(())
	}

	pub fn device_memory_to_string(&self, memory: vk::DeviceMemory, size: vk::DeviceSize) -> VkResult<String> {
		let ptr = unsafe {self.fns.map_memory(memory, 0, size, vk::MemoryMapFlags::empty())?};

		let binary = util::string_binary(ptr, size as usize);

		Ok(binary)
	}
}

pub mod util {
	use std::os::raw::c_void;

	pub fn string_binary(data: *mut c_void, size: usize) -> String {
		// let binary = String::new();
		unsafe {std::slice::from_raw_parts_mut(data as *mut u8, size)}.iter()
			.map(|byte| format!("{:08b}", byte))
				.collect::<Vec<String>>()
				.join(", ")
	}
}

pub struct Renderer<'a> {
	pub device: &'a ComputeDevice<'a>,
	pub swapchain: &'a Swapchain<'a>,

	pub command_pool: vk::CommandPool,
	pub queue: vk::Queue,
	pub render_pipeline: RenderPipeline<'a>,

	pub image_available_semaphores: Vec<vk::Semaphore>,
	pub render_finished_semaphores: Vec<vk::Semaphore>,
	pub rendering_fences: Vec<vk::Fence>,

	current_frame: Arc<RwLock<usize>>,
	max_concurrent_frames: usize,
}

impl<'a> Renderer<'a> {
	pub fn new(command_pool: vk::CommandPool, queue: vk::Queue,
		   swapchain: &'a Swapchain<'a>, render_scene: &'a RenderScene<'a>) -> ComputeResult<Renderer<'a>> {
	
		let device = swapchain.compute_device;

		let max_concurrent_frames = swapchain.frame_count;

		let render_pipeline = RenderPipeline::new(device, 
							  command_pool, queue, 
							  swapchain, max_concurrent_frames, render_scene)?;
		
		let (image_available_semaphores, render_finished_semaphores, rendering_fences) 
			= Self::make_prims(device, swapchain.frame_count);

		Ok(Renderer { device,	swapchain, 
			command_pool,
			queue,
			render_pipeline,
			image_available_semaphores, render_finished_semaphores, rendering_fences,
			current_frame: Arc::new(RwLock::new(0)),
			max_concurrent_frames,
		})
	}
	
	
	pub fn make_prims(device: & ComputeDevice, frame_count: usize) 
		-> (Vec<vk::Semaphore>, Vec<vk::Semaphore>, Vec<vk::Fence>)	{

		let semaphore_info = vk::SemaphoreCreateInfo::default();
		println!("semaphore created with info: {:?}", semaphore_info);
		let fence_info = vk::FenceCreateInfo {
			flags: vk::FenceCreateFlags::SIGNALED,
			..Default::default()
		};

		
		let mut ret = (
			Vec::with_capacity(frame_count), 
			Vec::with_capacity(frame_count), 
			Vec::with_capacity(frame_count));

		for i in 0..frame_count { unsafe {
			ret.0.push( device.fns.create_semaphore(&semaphore_info, None).unwrap());
			ret.1.push( device.fns.create_semaphore(&semaphore_info, None).unwrap());
			ret.2.push( device.fns.create_fence(&fence_info, None).unwrap());
		} }

		ret	
	}


	pub fn render(& self) -> RenderResult<()> {
		let Renderer {device, swapchain, ..} = self;

		let current_frame = self.current_frame.read().unwrap().clone();

		let next_frame = (current_frame+1) % self.max_concurrent_frames;
		(*self.current_frame.write().unwrap()) = next_frame;

		let image_available_semaphore = self.image_available_semaphores[current_frame];
		let render_finished_semaphore = self.render_finished_semaphores[current_frame];
		let render_fence = self.rendering_fences[current_frame];

		unsafe { device.fns.wait_for_fences(&self.rendering_fences, true, u64::MAX); }

		let res = unsafe { swapchain.acquire_next_image(image_available_semaphore) };
		println!("image semaphores: {:?}", res);
		let (image_index, swapchain_suboptimal) = res.unwrap();

		// let (image_index, swapchain_suboptimal) = unsafe {
			// swapchain.acquire_next_image(image_available_semaphore).unwrap() };

		if swapchain_suboptimal {
			// self.current_frame = 0;
			return Err(RenderError::SwapchainSuboptimal);
		}


		println!("Rendering image image: ");
		let res = self.render_pipeline.render(render_fence, image_available_semaphore, render_finished_semaphore
										, image_index, current_frame);
		println!("Rendering image res: {:?}", res);

		println!("Presenting image: ");
		let present_res = unsafe {swapchain.present_image(image_index, render_finished_semaphore)}.as_ref();

		// if present_res.is_ok_and(|suboptimal| *suboptimal)
			// || present_res.is_err_and(|err| *err == ComputeError::VkError(vk::Result::ERROR_OUT_OF_DATE_KHR)) {
				// return Err(RenderError::SwapchainSuboptimal);
		// }
		// else if present_res.is_err() {return Err(present_res.unwrap_err().into());}

		Ok(())

		// BufferData<UniformBufferObject> world_ubo;
		// updateWorld(render_scene, world_ubo);
		// render_scene->uniform0_description = &world_ubo;
		// render_scene->uniform0 = &world_ubo;
//
		// render_pipeline->drawScene(render_fence, image_available_semaphore, render_finished_semaphore
				// , image_i, current_frame
				// );
//
		// result = swap_chain->presentImage(&image_i, render_finished_semaphore);
		// if(result == SwapChain::Result::ERROR_OUTDATED || result == SwapChain::Result::SUBOPTIMAL
				// || framebuffer_resized) {
			// framebuffer_resized = false;
			// refreshRenderContextPipelineSwapChain(device, swap_chain, render_pipeline);
		// }
		// else if (result >= SwapChain::ERROR) std::runtime_error("failed to present swap chain image!");
		
	}

	// pub fn refresh_prims() create new & swap
}

#[derive(Debug)]
pub enum RenderError {
	SwapchainSuboptimal,
	ComputeError(ComputeError),
}

impl Into<RenderError> for ComputeError {
	fn into(self) -> RenderError {
		RenderError::ComputeError(self)
	}
}

pub type RenderResult<T> = Result<T, RenderError>;

pub struct DataFrame {
	pub buffers: Vec<vk::Buffer>,
	pub memory: Vec<vk::DeviceMemory>,
	pub size: usize,
	pub frame_count: usize,
}

impl<'a> DataFrame {
    pub fn new(device: &'a ComputeDevice<'a>, size: usize, frame_count: usize) -> Self { 

		let mut buffers = Vec::with_capacity(frame_count);
		let mut memories = Vec::with_capacity(frame_count);

		for i in 0..frame_count {
			let (buffer, memory) = unsafe {device.create_buffer(size as vk::DeviceSize, vk::BufferUsageFlags::UNIFORM_BUFFER, 
								  vk::SharingMode::EXCLUSIVE, 
								  vk::MemoryPropertyFlags::HOST_COHERENT | vk::MemoryPropertyFlags::HOST_VISIBLE)
			}.unwrap();

			buffers.push(buffer);
			memories.push(memory);
		}
		Self { size, buffers, memory: memories, frame_count }
	}

	pub fn update_memory(&self, device: &'a ComputeDevice<'a>, buffer: & HostBufferPtr, frame: usize) -> VkResult<()> {
		if buffer.size > self.size {panic!("Src data bigger than DataFrame buffer")}

		unsafe{ device.set_device_memory(buffer.data as *const c_void, buffer.size as vk::DeviceSize, 0, self.memory[frame as usize])?};

		let data_slice = unsafe { std::slice::from_raw_parts(buffer.data, buffer.size) };
		// println!("Current Buffer update data: {:?}", data_slice);

		Ok(())
	}
}

// Evolve into RenderGraph::from(RenderFeaturesGraph::from(WorldGraph || RenderScene))
pub struct RenderPipeline<'a> {
	pub(crate) device: &'a  ComputeDevice<'a>,
	pub(crate) swapchain: &'a Swapchain<'a>,

	pub(crate) queue: vk::Queue,
	pub(crate) pool: vk::DescriptorPool,

	pub(crate) frame_count: usize,

	pub(crate) render_scene: &'a RenderScene<'a>,
	pub(crate) world_ubo: DataFrame, // built around render scene buffers
	pub(crate) entity_ubo: DataFrame,
	pub(crate) binding_offset: usize,

	pub(crate) command_buffers: Vec<vk::CommandBuffer>,
	pub(crate) render_pass: vk::RenderPass,
	pub(crate) graphics_pipeline: vk::Pipeline,
	pub(crate) graphics_pipeline_layout: vk::PipelineLayout,
	pub(crate) framebuffers: Vec<vk::Framebuffer>,
	pub(crate) descriptor_set_layout: vk::DescriptorSetLayout,
	pub(crate) descriptor_sets: Vec<vk::DescriptorSet>,
	pub(crate) vertex_binding_descriptions: Vec<vk::VertexInputBindingDescription>,
	pub(crate) vertex_attribute_descriptions: Vec<vk::VertexInputAttributeDescription>,

	pub(crate) vert_shader: vk::ShaderModule,
	pub(crate) frag_shader: vk::ShaderModule,

}

impl<'a> RenderPipeline<'a> {
	pub fn new(device: &'a	ComputeDevice<'a>, 
		   command_pool: vk::CommandPool,
		   queue: vk::Queue,
		   swapchain: &'a Swapchain<'a>, 
		   frame_count: usize, 
		   render_scene: &'a RenderScene<'a>) -> ComputeResult<Self> {

		println!("world-ubo length {:?}", render_scene.world_ubo.size);
		println!("entity-ubo length {:?}", render_scene.entity_ubo.size);
		let world_ubo = DataFrame::new(device, render_scene.world_ubo.size, frame_count);
		let entity_ubo = DataFrame::new(device, render_scene.entity_ubo.size, frame_count);

		let binding_offset = Self::pad_min_byte_size(
			render_scene.entity_ubo.stride, 
			device.physical_device.properties.limits.min_uniform_buffer_offset_alignment as usize);
		println!("binding_offset : {:?}", binding_offset);
		println!("device.physical_device.properties.limits.min_uniform_buffer_offset_alignment : {:?}", 
			 device.physical_device.properties.limits.min_uniform_buffer_offset_alignment);

		let command_buffers = device.allocate_command_buffers(command_pool, frame_count as u32)?;

		let scene_material = &render_scene.render_components[0].render_type.material;
		let texture = scene_material.texture.device_data.as_ref().unwrap();
		let pool = Self::create_descriptor_pool(device, frame_count as u32)?;
		let descriptor_set_layout = Self::create_descriptor_set_layout(device)?;
		let descriptor_sets = Self::create_descriptor_sets(device, pool, frame_count, 
								   descriptor_set_layout, 
								   &world_ubo, 
								   &entity_ubo, 
								   binding_offset,
								   texture.sampler, 
								   texture.image_view)?;

		let vertex_descriptions = vec![
			render_scene.render_components[0].render_type.mesh.vertices.vertex_description.clone()
		];
		let (vertex_binding_descriptions, vertex_attribute_descriptions)
			= Self::get_vertex_input_descriptors(&vertex_descriptions);
		Self::print_vertex_input_descriptors(&vertex_binding_descriptions, &vertex_attribute_descriptions);

		let vert_shader = scene_material.vert_shader.module.as_ref().unwrap().module;
		let frag_shader = scene_material.frag_shader.module.as_ref().unwrap().module;

		let render_pass = Self::create_render_pass(device, swapchain.image_format, swapchain.depth_format)?;
		let framebuffers = Self::create_render_pipeline_framebuffers(device, render_pass, swapchain, frame_count)?;
		let (graphics_pipeline, graphics_pipeline_layout) 
			= Self::create_graphics_pipeline(device, swapchain, render_pass, descriptor_set_layout, 
							 &vertex_binding_descriptions, 
							 &vertex_attribute_descriptions, 
							 vert_shader, frag_shader)?;

		println!("Created graphics pipeline!");

		Ok(Self {
			device,
			swapchain,

			queue,
			pool,

			frame_count,
			render_scene,
			world_ubo,
			entity_ubo,
			binding_offset,

			command_buffers,
			render_pass,
			graphics_pipeline,
			graphics_pipeline_layout,
			framebuffers,
			descriptor_set_layout,
			descriptor_sets,

			vertex_attribute_descriptions,
			vertex_binding_descriptions,

			vert_shader,
			frag_shader,
		})
	}

	pub fn pad_min_byte_size(min_alignment: usize, original_alignment: usize) -> usize {
		if min_alignment == 0 {return original_alignment};

		(original_alignment + min_alignment - 1) & !(min_alignment-1)
	}

	// submits rendering operation
	pub fn render(&self, render_fence: vk::Fence, 
		      image_available_semaphore: vk::Semaphore, 
		      render_finished_semaphore: vk::Semaphore, 
		      image_index: u32, current_frame: usize) -> ComputeResult<()> {
		
		self.world_ubo.update_memory(self.device, &self.render_scene.world_ubo, current_frame)?;
		self.entity_ubo.update_memory(self.device, &self.render_scene.entity_ubo, current_frame)?;

		let command_buffer = self.command_buffers[current_frame];
		let src_extent = &self.render_scene.render_extent;
		let render_extent = vk::Extent2D {
			width: src_extent[0],
			height: src_extent[1],
		};
		
		self.record_draw_command_buffer(command_buffer, 
						self.descriptor_sets[current_frame], 
						self.framebuffers[current_frame], 
						render_extent, 
						&self.render_scene.render_components, 
						self.binding_offset);

		let submit_info = vk::SubmitInfo::builder()
			.wait_semaphores(&[image_available_semaphore])
			.wait_dst_stage_mask(&[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
			.command_buffers(&[command_buffer])
			.signal_semaphores(&[render_finished_semaphore])
			.build();

		unsafe{self.device.fns.reset_fences(&[render_fence])}?;
		unsafe{self.device.fns.queue_submit(self.queue, &[submit_info], render_fence)?};

		Ok(())
	}

	pub fn record_draw_command_buffer(&self, 
					  command_buffer: vk::CommandBuffer, 
					  descriptor_set: vk::DescriptorSet,
					  framebuffer: vk::Framebuffer,
					  render_area_extent: vk::Extent2D,
					  render_components: &Vec<RenderComponent>,
					  binding_offset: usize) -> ComputeResult<()> {
		let cmd_begin_info = vk::CommandBufferBeginInfo::default();
		unsafe{self.device.fns.begin_command_buffer(command_buffer, &cmd_begin_info)?};

		let clear_values = [
			vk::ClearValue {
				color: vk::ClearColorValue { float32: ([0.1, 0.1, 0.1, 1.0]) }
			},
			vk::ClearValue {
				depth_stencil: vk::ClearDepthStencilValue { depth: 1.0,
					stencil: 0,
				}
			},
		];

		let render_pass_begin = vk::RenderPassBeginInfo::builder()
			.render_pass(self.render_pass)
			.framebuffer(framebuffer)
			.render_area(vk::Rect2D {
				offset: vk::Offset2D::default(),
				extent: render_area_extent,
			})
			.clear_values(&clear_values)
			.build();
		unsafe{self.device.fns.cmd_begin_render_pass(command_buffer, 
							     &render_pass_begin, 
							     vk::SubpassContents::INLINE)};
		unsafe{self.device.fns.cmd_bind_pipeline(command_buffer, 
							 vk::PipelineBindPoint::GRAPHICS, 
							 self.graphics_pipeline)};

		println!("entity loop!");
		//////////////////////// ENTITY_LOOP
		for (index, component) in render_components.iter().enumerate() {
			// if index > 10 {continue}

			let vertex_buffers = &[
				component.render_type.mesh.vertices.device_data.as_ref().unwrap().buffer];
			let indices = &component.render_type.mesh.indices;
			let index_buffer = indices.device_data.as_ref().unwrap().buffer;

			unsafe{self.device.fns.cmd_bind_vertex_buffers(command_buffer, 0, vertex_buffers, &[0])};
			unsafe{self.device.fns.cmd_bind_index_buffer(command_buffer,
								     index_buffer, 0, vk::IndexType::UINT16)};

			let dynamic_offsets = [(binding_offset*index) as u32];
			// println!("dynamic_offsets: {:?}", dynamic_offsets);
			unsafe{self.device.fns.cmd_bind_descriptor_sets(command_buffer,
									vk::PipelineBindPoint::GRAPHICS,
									self.graphics_pipeline_layout,
									0,
									&[descriptor_set],
									&dynamic_offsets)};
			unsafe{self.device.fns.cmd_draw_indexed(command_buffer, indices.size as u32, 1, 0, 0, 0)};
			// unsafe {self.device.fns.cmd_draw(command_buffer, 3, 1, 0, 0)};
		}

		unsafe{self.device.fns.cmd_end_render_pass(command_buffer)};

		unsafe{self.device.fns.end_command_buffer(command_buffer)?};

		Ok(())
	}

	pub fn create_graphics_pipeline(device: &'a ComputeDevice<'a>,
					swapchain: &'a Swapchain<'a>,
					render_pass: vk::RenderPass, 
					descriptor_set_layout: vk::DescriptorSetLayout, 
					vertex_binding_descriptions: &Vec<vk::VertexInputBindingDescription>, 
					vertex_attribute_descriptions: &Vec<vk::VertexInputAttributeDescription>, 
					vert_shader: vk::ShaderModule, frag_shader: vk::ShaderModule)
	-> VkResult<(vk::Pipeline, vk::PipelineLayout)> {
		let extent = &swapchain.extent;

		let stages_info = [
			vk::PipelineShaderStageCreateInfo {
				stage: vk::ShaderStageFlags::VERTEX,
				module: vert_shader,
				// p_name: "main".as_ptr() as *const i8,
				p_name: unsafe{CStr::from_ptr("main\0".as_ptr() as *const i8).as_ptr()},
				..Default::default()
			},
			vk::PipelineShaderStageCreateInfo {
				stage: vk::ShaderStageFlags::FRAGMENT,
				module: frag_shader,
				// p_name: "main".as_ptr() as *const i8,
				p_name: unsafe{CStr::from_ptr("main\0".as_ptr() as *const i8).as_ptr()},
				..Default::default()
			},
		];

		let vertex_input_state = vk::PipelineVertexInputStateCreateInfo::builder()
			.vertex_binding_descriptions(vertex_binding_descriptions)
			.vertex_attribute_descriptions(vertex_attribute_descriptions)
			// .vertex_binding_descriptions(&[])
			// .vertex_attribute_descriptions(&[])
			.build();
		let input_assembly_state = vk::PipelineInputAssemblyStateCreateInfo {
			topology: vk::PrimitiveTopology::TRIANGLE_LIST,
			primitive_restart_enable: vk::FALSE,
			..Default::default()
		};
		let viewport = vk::Viewport {
			x: 0.0,
			y: 0.0,
			width: extent.width as f32,
			height: extent.height as f32,
			min_depth: 0.0,
			max_depth: 1.0,
		};
		let scissor = vk::Rect2D {
			offset: vk::Offset2D {
				x: 0, y: 0,
			},
			extent: *extent,
		};
		let viewport_state = vk::PipelineViewportStateCreateInfo::builder()
			.viewports(&[viewport])
			.scissors(&[scissor])
			.build();
		let rasterization_state = vk::PipelineRasterizationStateCreateInfo {
			depth_clamp_enable: vk::FALSE,
			rasterizer_discard_enable: vk::FALSE,
			polygon_mode: vk::PolygonMode::FILL,
			// cull_mode: vk::CullModeFlags::BACK,
			cull_mode: vk::CullModeFlags::NONE,
			front_face: vk::FrontFace::COUNTER_CLOCKWISE,
			// front_face: vk::FrontFace::CLOCKWISE,
			depth_bias_enable: vk::FALSE,
			depth_bias_constant_factor: 0.0,
			depth_bias_clamp: 0.0,
			line_width: 1.0,
			..Default::default()
		};
		let multisample_state = vk::PipelineMultisampleStateCreateInfo {
			rasterization_samples: vk::SampleCountFlags::TYPE_1,
			sample_shading_enable: vk::FALSE,
			min_sample_shading: 1.0,
			p_sample_mask: 0 as *const u32,
			alpha_to_coverage_enable: vk::FALSE,
			alpha_to_one_enable: vk::FALSE,
			..Default::default()
		};
		let depth_stencil_state = vk::PipelineDepthStencilStateCreateInfo {
			depth_test_enable: vk::TRUE,
			depth_write_enable: vk::TRUE,
			depth_compare_op: vk::CompareOp::LESS,
			depth_bounds_test_enable: vk::FALSE,
			stencil_test_enable: vk::FALSE,
			front: vk::StencilOpState::default(),
			back: vk::StencilOpState::default(),
			min_depth_bounds: 0.0,
			max_depth_bounds: 0.0,
			..Default::default()
		};
		let color_blend_attachment = vk::PipelineColorBlendAttachmentState {
			blend_enable: vk::TRUE,
			color_write_mask: vk::ColorComponentFlags::R 
				| vk::ColorComponentFlags::G | vk::ColorComponentFlags::B | vk::ColorComponentFlags::A,
			// optional
			src_color_blend_factor: vk::BlendFactor::ONE,
			dst_color_blend_factor: vk::BlendFactor::ZERO,
			color_blend_op: vk::BlendOp::ADD,
			src_alpha_blend_factor: vk::BlendFactor::ONE,
			dst_alpha_blend_factor: vk::BlendFactor::ZERO,
			alpha_blend_op: vk::BlendOp::ADD,
			/*
			// alpha handling
			blend_enable: vk::TRUE,
			src_color_blend_factor: vk::BlendFactor::ALPHA,
			dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
			color_blend_op: vk::BlendOp::ADD,
			src_alpha_blend_factor: vk::BlendFactor::ONE,
			dst_alpha_blend_factor: vk::BlendFactor::ZERO,
			alpha_blend_op: vk::BlendOp::ADD,
			 */
			..Default::default()
		};
		let color_blend_state = vk::PipelineColorBlendStateCreateInfo::builder()
			.logic_op_enable(false)
			.logic_op(vk::LogicOp::COPY)
			.attachments(&[color_blend_attachment])
			.blend_constants([0.0, 0.0, 0.0, 0.0])
			.build();

		let descriptor_set_layouts = vec![descriptor_set_layout];
		let pipeline_layout_info = vk::PipelineLayoutCreateInfo::builder()
			.set_layouts(&descriptor_set_layouts)
			// .set_layouts(&[])
			.push_constant_ranges(&[])
			.build();

		let graphics_pipeline_layout = unsafe {
			device.fns.create_pipeline_layout(&pipeline_layout_info, None)?
		};

		let pipeline_info = vk::GraphicsPipelineCreateInfo::builder()
			.stages(&stages_info)
			.vertex_input_state(&vertex_input_state)
			.input_assembly_state(&input_assembly_state)
			.viewport_state(&viewport_state)
			.rasterization_state(&rasterization_state)
			.multisample_state(&multisample_state)
			.depth_stencil_state(&depth_stencil_state)
			.color_blend_state(&color_blend_state)
			// .dynamic_state(dynamic_state)
			.layout(graphics_pipeline_layout)
			.render_pass(render_pass)
			.subpass(0)
			.base_pipeline_handle(vk::Pipeline::null())
			.base_pipeline_index(-1)
			.build();

		let graphics_pipeline = unsafe {
			device.fns.create_graphics_pipelines(vk::PipelineCache::null(), &[pipeline_info], None)
		};

		let graphics_pipeline = match graphics_pipeline {
			Ok(v) => Ok(v[0]),
			Err(e) => Err(e.1),
		}?;

		Ok((graphics_pipeline, graphics_pipeline_layout))
	}

	pub fn create_render_pipeline_framebuffers(device: &'a ComputeDevice<'a>, 
						   render_pass: vk::RenderPass, swapchain: &'a Swapchain<'a>, 
						   frame_count: usize) 
	-> VkResult<Vec<vk::Framebuffer>> {
		let mut framebuffers = Vec::with_capacity(frame_count);

		for frame in 0..frame_count {
			let attachments = vec![swapchain.image_views[frame], swapchain.depth_image_view];
			let extent = &swapchain.extent;

			let framebuffer_info = vk::FramebufferCreateInfo::builder()
				.render_pass(render_pass)
				.attachments(&attachments)
				.width(extent.width)
				.height(extent.height)
				.layers(1)
				.build();

			framebuffers.push(unsafe{device.fns.create_framebuffer(&framebuffer_info, None)?});
		}

		Ok(framebuffers)
	}

	pub fn create_render_pass(device: &'a ComputeDevice<'a>, color_format: vk::Format, depth_format: vk::Format) 
	-> ComputeResult<vk::RenderPass> {
		let color_attachment = vk::AttachmentDescription {
			format: color_format,
			samples: vk::SampleCountFlags::TYPE_1,
			load_op: vk::AttachmentLoadOp::CLEAR,
			store_op: vk::AttachmentStoreOp::STORE,
			stencil_load_op: vk::AttachmentLoadOp::DONT_CARE,
			stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
			initial_layout: vk::ImageLayout::UNDEFINED,
			final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
			..Default::default()
		};

		let depth_attachment = vk::AttachmentDescription {
			format: depth_format,
			samples: vk::SampleCountFlags::TYPE_1,
			load_op: vk::AttachmentLoadOp::CLEAR,
			store_op: vk::AttachmentStoreOp::DONT_CARE,
			stencil_load_op: vk::AttachmentLoadOp::CLEAR,
			stencil_store_op: vk::AttachmentStoreOp::DONT_CARE,
			initial_layout: vk::ImageLayout::UNDEFINED,
			final_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			..Default::default()
		};

		let subpass = vk::SubpassDescription::builder()
			.pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
			.color_attachments(&[vk::AttachmentReference {
				attachment: 0,
				layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
			}])
			.depth_stencil_attachment(&vk::AttachmentReference {
				attachment: 1,
				layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
			})
			.build();

		let dependency = vk::SubpassDependency {
			src_subpass: vk::SUBPASS_EXTERNAL,
			dst_subpass: 0,
			src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT 
							| vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
			dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT 
							| vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS,
			src_access_mask: vk::AccessFlags::empty(),
			dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_WRITE
				| vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
			..Default::default()
		};

		let renderpass_info = vk::RenderPassCreateInfo::builder()
			.attachments(&[color_attachment, depth_attachment])
			.subpasses(&[subpass])
			.dependencies(&[dependency])
			.build();

		println!("Creating subpass!");
		let res = Ok(unsafe{device.fns.create_render_pass(&renderpass_info, None)}?);
		println!("Subpass created!!");
		res
	}

	pub fn get_vertex_input_descriptors(vertex_description_list: &Vec<VertexInputDescription>)
	-> (Vec<vk::VertexInputBindingDescription>, Vec<vk::VertexInputAttributeDescription>){

		let mut binding_descriptions = Vec::with_capacity(vertex_description_list.len());
		let mut attribute_descriptions = vec![];
		let attributes_offset = 0;

		for (binding, vertex_description) in vertex_description_list.iter().enumerate() {

			binding_descriptions.push(vk::VertexInputBindingDescription {
				binding: binding as u32,
				stride: vertex_description.stride as u32,
				input_rate: vk::VertexInputRate::VERTEX,
			});
			// binding_descriptions[binding] = vk::VertexInputBindingDescription {
				// binding: binding as u32,
				// stride: vertex_description.stride as u32,
				// input_rate: vk::VertexInputRate::VERTEX,
			// };

			let attributes_len = vertex_description.attributes.len();
			attribute_descriptions.reserve(attributes_offset + attributes_len);

			for att_i in 0..attributes_len {
				
				attribute_descriptions.push(vk::VertexInputAttributeDescription {
					location: (attributes_offset+att_i) as u32,
					binding: binding as u32,
					format: vertex_description.attributes[att_i].format.clone().into(),
					offset: vertex_description.attributes[att_i].offset as u32,
				});
				// attribute_descriptions[attributes_offset+att_i] = vk::VertexInputAttributeDescription {
					// location: (attributes_offset+att_i) as u32,
					// binding: binding as u32,
					// format: vertex_description.attributes[att_i].format.clone().into(),
					// offset: vertex_description.attributes[att_i].offset as u32,
				// };
			}
		}

		// println!("Vertex Des")

		(binding_descriptions, attribute_descriptions)
	}
	pub fn print_vertex_input_descriptors(
		vertex_binding_descriptions: &Vec<vk::VertexInputBindingDescription>,
		vertex_attribute_descriptions: &Vec<vk::VertexInputAttributeDescription>,
	) {
		println!("{:?}", vertex_binding_descriptions);
		println!("{:?}", vertex_attribute_descriptions);
	}

	// Creates and writes descriptor sets
	// TODO: implement on RenderScene descriptors graph
	pub fn create_descriptor_sets(device: &'a ComputeDevice<'a>, pool: vk::DescriptorPool, frame_count: usize, 
				    descriptor_set_layout: vk::DescriptorSetLayout, 
				    data_frame: &DataFrame, 
				    position_cache: &DataFrame, 
				    entity_binding_offset: usize,
				    texture_sampler: vk::Sampler, 
				    texture_image_view: vk::ImageView) -> VkResult<Vec<vk::DescriptorSet>> {
		
		let set_layouts = vec![descriptor_set_layout; frame_count];
		let alloc_info = vk::DescriptorSetAllocateInfo::builder()
			.descriptor_pool(pool)
			.set_layouts(&set_layouts)
			.build();

		let descriptor_sets = unsafe{device.fns.allocate_descriptor_sets(&alloc_info)?};

		for frame in 0..frame_count {
			let world_buffer_info = vk::DescriptorBufferInfo {
				buffer: data_frame.buffers[frame],
				offset: 0,
				range: data_frame.size as u64,
			};
			let entity_buffer_info = vk::DescriptorBufferInfo {
				buffer: position_cache.buffers[frame],
				offset: 0,
				range: entity_binding_offset as u64,
			};
			let image_info = vk::DescriptorImageInfo {
				sampler: texture_sampler,
				image_view: texture_image_view,
				image_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
			};

			let descriptor_writes = vec![
				vk::WriteDescriptorSet {
					dst_set: descriptor_sets[frame],
					dst_binding: 0,
					dst_array_element: 0,
					descriptor_count: 1,
					descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
					p_buffer_info: &world_buffer_info,
					..Default::default()
				},
				vk::WriteDescriptorSet {
					dst_set: descriptor_sets[frame],
					dst_binding: 1,
					dst_array_element: 0,
					descriptor_count: 1,
					descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
					p_image_info: &image_info,
					..Default::default()
				},
				vk::WriteDescriptorSet {
					dst_set: descriptor_sets[frame],
					dst_binding: 2,
					dst_array_element: 0,
					descriptor_count: 1,
					descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
					p_buffer_info: &entity_buffer_info,
					..Default::default()
				},
			];

			unsafe{device.fns.update_descriptor_sets(&descriptor_writes, &[])};
		}

		Ok(descriptor_sets)
	}

	// TODO: derive descriptor set layout from scene descriptor sets graph
	pub fn create_descriptor_set_layout(device: &'a ComputeDevice<'a>) -> VkResult<vk::DescriptorSetLayout> {
		let ubo_layout_binding = vk::DescriptorSetLayoutBinding {
			binding: 0,
			descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
			descriptor_count: 1,
			stage_flags: vk::ShaderStageFlags::VERTEX,
			..Default::default()
		};
		let texture_sampler_layout_binding = vk::DescriptorSetLayoutBinding {
			binding: 1,
			descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
			descriptor_count: 1,
			stage_flags: vk::ShaderStageFlags::FRAGMENT,
			..Default::default()
		};
		let position_layout_binding = vk::DescriptorSetLayoutBinding {
			binding: 2,
			descriptor_type: vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
			descriptor_count: 1,
			stage_flags: vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT,
			..Default::default()
		};

		let layout_info = vk::DescriptorSetLayoutCreateInfo::builder()
			.bindings(&[
					  ubo_layout_binding,
					  texture_sampler_layout_binding,
					  position_layout_binding])
			.build();

		Ok(unsafe {device.fns.create_descriptor_set_layout(&layout_info, None)?})
	}

	// TODO: derive from scene descriptors graph
	pub fn create_descriptor_pool(device: &'a ComputeDevice<'a>, frame_count: u32) -> VkResult<vk::DescriptorPool> {
		let frame_count = frame_count as u32;
		let pool_sizes = vec![
			vk::DescriptorPoolSize {
				ty : vk::DescriptorType::UNIFORM_BUFFER,
				descriptor_count: (frame_count * 30) as u32,
			},
			vk::DescriptorPoolSize {
				ty : vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
				descriptor_count: (frame_count * 30) as u32,
			},
			vk::DescriptorPoolSize {
				ty : vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC,
				descriptor_count: (frame_count * 30) as u32,
			},
		];

		let create_info = vk::DescriptorPoolCreateInfo::builder()
			.max_sets(frame_count)
			.pool_sizes(&pool_sizes)
			.build();

		Ok(unsafe{device.fns.create_descriptor_pool(&create_info, None)?})
	}

}

// immutable pointers to mutating data
pub struct RenderScene<'a> {
	pub entity_ubo: HostBufferPtr,
	pub world_ubo: HostBufferPtr,
	pub render_components: Vec<RenderComponent<'a>>,

	pub render_extent: [u32;2],
}

pub struct RenderComponent<'a> {
	pub render_type: RenderType<'a>,
}

pub struct Swapchain<'a> {
	pub compute_device: &'a ComputeDevice<'a>,
	pub device: &'a ash::Device,
	pub ext_khr_surface: &'a ash::extensions::khr::Surface,
	pub ext_khr_swapchain: &'a ash::extensions::khr::Swapchain,
	pub command_pool: &'a CommandPool<'a>,
	pub queue: &'a Queue<'a>,

	pub swapchain: vk::SwapchainKHR,

	pub surface: &'a vk::SurfaceKHR,
	pub surface_support: SwapchainSurfaceSupport,

	pub frame_count: usize,
	pub extent: vk::Extent2D,
	pub image_format: vk::Format,
	pub images: Vec<vk::Image>,
	pub image_views: Vec<vk::ImageView>,

	pub depth_format: vk::Format,
	pub depth_image: vk::Image,
	pub depth_image_memory: vk::DeviceMemory,
	pub depth_image_view: vk::ImageView,
}

impl<'a> Swapchain<'a> {

	pub fn new(device: &'a ComputeDevice<'a>, command_pool: &'a CommandPool, queue: &'a Queue, wsurface: &'a WindowSurface) 
		-> ComputeResult<Swapchain<'a>> 
	{
		let ComputeDevice {physical_device, ..} = device;

		let ext_khr_swapchain = device.ext_khr_swapchain.as_ref()
			.expect("Extension not enabled on device !! (SWAPCHAIN_KHR)");
		let ext_khr_surface = device.api_instance.ext_khr_surface.as_ref()
			.expect("Extension not enabled on device !! (SURFACE_KHR)");

		let swapchain_surface_support = SwapchainSurfaceSupport::new(ext_khr_surface, physical_device.handle, wsurface.surface);
		let swapchain_support = swapchain_surface_support.select_swapchain_support();
		// let swapchain_extent = vk::Extent2D {width: window_width, height: window_height };
		// let swapchain_extent = vk::Extent2D {width: wsurface.window.width , height: wsurface.window.height };
		let swapchain_extent = swapchain_surface_support.capabilities.max_image_extent;


		println!("swapchain image count: {:?}", swapchain_support.min_image_count);
		let swapchain_create_info = vk::SwapchainCreateInfoKHR {
			surface: wsurface.surface,
			min_image_count: swapchain_support.min_image_count,
			image_format: swapchain_support.format.format,
			image_color_space: swapchain_support.format.color_space,
			image_extent: swapchain_extent,
			// image_extent: swapchain_support.capabilities.max_image_extent, // TODO: fix, debug
			image_array_layers: 1,
			image_usage: vk::ImageUsageFlags::COLOR_ATTACHMENT,
															//
			pre_transform: swapchain_support.pre_transform,
			composite_alpha: vk::CompositeAlphaFlagsKHR::OPAQUE,
			present_mode: swapchain_support.present_mode,
			clipped: vk::TRUE,
			old_swapchain: vk::SwapchainKHR::null(),
			..Default::default()
		};

		// TODO: add multiqueue logic
		// let PhysicalDevice {queue_families, ..} = physical_device;

		// let queue_family_indices = physical_device.get_queue_families_index(
			// &[QueueType::Graphics, QueueType::Present])
			// .expect("Cant create Swapchain, incomplete queue families on device!");
//
		// if queue_family_indices.len() != 2 {
			// swapchain_create_info.image_sharing_mode = vk::SharingMode::CONCURRENT;
			// swapchain_create_info.queue_family_index_count = queue_family_indices.len() as u32;
			// swapchain_create_info.p_queue_family_indices = queue_family_indices.as_ptr();
		// } else {
			// swapchain_create_info.image_sharing_mode = vk::SharingMode::EXCLUSIVE;
		// }

		println!("sc create info : {:?}", swapchain_create_info);
		let swapchain = unsafe{ ext_khr_swapchain.create_swapchain(&swapchain_create_info, None) }
			.expect("Cant create swapchain!");

		let images = unsafe { ext_khr_swapchain.get_swapchain_images(swapchain) }
			.expect("Cant create swapchain images!");
		println!("created swapchain images: {:?}: {:?}", images.len(), images);

		let mut image_views = Vec::with_capacity(images.len());

		for image in images.iter() {
			image_views.push(device.create_image_view(
					*image, swapchain_support.format.format, vk::ImageAspectFlags::COLOR )?);
		}

		println!("Created swapchain images!");

		let depth_format = device.find_supported_format(
			&vec![vk::Format::D32_SFLOAT, vk::Format::D32_SFLOAT_S8_UINT, vk::Format::D24_UNORM_S8_UINT] ,
			vk::ImageTiling::OPTIMAL,
			vk::FormatFeatureFlags::DEPTH_STENCIL_ATTACHMENT,
			)?;

		let (depth_image, depth_image_memory) = unsafe {device.create_image(
			&swapchain_extent.into(), vk::ImageType::TYPE_2D, depth_format, vk::ImageTiling::OPTIMAL, 
			vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT,
			vk::SharingMode::EXCLUSIVE,
			vk::MemoryPropertyFlags::DEVICE_LOCAL
			)}?;

		let depth_image_view = device.create_image_view(depth_image, depth_format, vk::ImageAspectFlags::DEPTH)?;

		println!("starting transition!");
		device.transition_image_layout(command_pool.handle, queue.handle, depth_image, depth_format, 
									   vk::ImageLayout::UNDEFINED, 
									   vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL)?;
		println!("image transitioned!");

		Ok(Swapchain { 
			compute_device: device,
			device: &device.fns , ext_khr_surface, ext_khr_swapchain, 
			swapchain, 
			command_pool,
			queue,
			surface: &wsurface.surface,
			surface_support: swapchain_surface_support,
			frame_count: swapchain_support.min_image_count as usize,
			extent: swapchain_extent,
			image_format: swapchain_support.format.format,
			depth_format,
			images, image_views,
			depth_image, depth_image_memory, depth_image_view,
		})

	}
	// the bool indicates if swapchain is suboptimal
	pub unsafe fn acquire_next_image(&self, available_semaphore: vk::Semaphore) -> ComputeResult<(u32, bool)> {
		Ok(self.ext_khr_swapchain.acquire_next_image(self.swapchain, u64::MAX, available_semaphore, vk::Fence::null())?)
	}

	// the bool indicates if swapchain is suboptimal
	pub unsafe fn present_image(&self, image_index: u32, wait_sempahore: vk::Semaphore) -> ComputeResult<bool> {
		let present_info = vk::PresentInfoKHR::builder()
			.wait_semaphores(&[wait_sempahore])
			.swapchains(&[self.swapchain])
			.image_indices(&[image_index])
			.build();
		println!("image_index: {:?}", image_index);
		println!("PresentInfoKHR: {:?}", present_info);

		Ok(self.ext_khr_swapchain.queue_present(self.queue.handle, &present_info)?)
		// Ok(self.compute_device.fns.queue_present(self.queue.handle, &present_info)?)
	}
			// Result presentImage(uint32_t * image_index, VkSemaphore wait_sempahore) {
//
		// VkSemaphore wait_sempahores[] {wait_sempahore};
		// VkPresentInfoKHR present_info {
			  // .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR
			// , .waitSemaphoreCount = 1
			// , .pWaitSemaphores = wait_sempahores
			// , .swapchainCount = 1
			// , .pSwapchains = &swap_chain
			// , .pImageIndices = image_index
			// , .pResults = nullptr // optional
		// };
		// VkResult result = vkQueuePresentKHR(device->present_queue, &present_info);
//
		// if(result == VK_ERROR_OUT_OF_DATE_KHR) return Result::ERROR_OUTDATED;
		// if(result == VK_SUBOPTIMAL_KHR) return Result::SUBOPTIMAL;
		// if(result == VK_SUCCESS) return Result::SUCCESS;
		// else return Result::ERROR;
	// }
//

	pub fn refresh() {
		todo!()
	}
}

pub struct SwapchainSupport {
	pub capabilities: vk::SurfaceCapabilitiesKHR,
	pub present_mode: vk::PresentModeKHR,
	pub format: vk::SurfaceFormatKHR,
	pub min_image_count: u32,
	pub pre_transform: vk::SurfaceTransformFlagsKHR,
}

pub struct SwapchainSurfaceSupport {
	pub capabilities: vk::SurfaceCapabilitiesKHR,
	pub present_modes: Vec<vk::PresentModeKHR>,
	pub formats: Vec<vk::SurfaceFormatKHR>,
}

impl SwapchainSurfaceSupport {

	pub fn new(surface_loader: &khr::Surface, device: vk::PhysicalDevice, surface: vk::SurfaceKHR) -> SwapchainSurfaceSupport{ unsafe {

		let formats = surface_loader.get_physical_device_surface_formats(device, surface)
				.expect("Cant get surface formats");
		let present_modes = surface_loader.get_physical_device_surface_present_modes(device, surface)
				.expect("Cant get surface present modes");

		let capabilities = surface_loader.get_physical_device_surface_capabilities(device, surface)
			.expect("Cant get surface capabilities");

		SwapchainSurfaceSupport { capabilities, present_modes, formats }
	}}

	pub fn select_swapchain_support(&self) -> SwapchainSupport {

		let present_mode = self.present_modes.iter().find(|&&mode| mode == vk::PresentModeKHR::MAILBOX)
			.unwrap_or(&vk::PresentModeKHR::FIFO).clone();

		let format = self.formats.iter()
			.find(|&&format| 
				  format.format == vk::Format::B8G8R8A8_SRGB 
				  && format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
				  )
			.unwrap_or(&self.formats[0]).clone();

		println!("min_image_count, max_image_count: {}, {}", self.capabilities.min_image_count, self.capabilities.max_image_count);

		// TODO : check best image count: https://arm-software.github.io/vulkan_best_practice_for_mobile_developers/samples/performance/swapchain_images/swapchain_images_tutorial.html
		// let max_image_count = self.capabilities.max_image_count;
//
		// let min_img_count = self.capabilities.min_image_count + 1;
		// let max_img_count = 3;
//
		// let image_count = std::cmp::max(
			// min_img_count,
			// std::cmp::min(max_img_count, if max_image_count == 0 {max_img_count} else {max_image_count}));
		
		let max_image_count = if self.capabilities.max_image_count == 0 {3} else {std::cmp::min(self.capabilities.max_image_count, 3)};

		let image_count = std::cmp::max(self.capabilities.min_image_count, max_image_count);

		println!("selected image_count: {}", image_count);

		// TODO: (debug) inspect current transform, compare
		// let pre_transform = if self.capabilities.supported_transforms.contains(vk::SurfaceTransformFlagsKHR::IDENTITY)
			// {vk::SurfaceTransformFlagsKHR::IDENTITY} else {self.capabilities.current_transform};
		let pre_transform = self.capabilities.current_transform;

		println!("min_image_extent, max_image_extent: {:?}, {:?}", self.capabilities.min_image_extent, self.capabilities.max_image_extent);

		SwapchainSupport { 
			capabilities: self.capabilities.clone(), 
			present_mode, 
			format, 
			min_image_count: image_count,
			pre_transform,
		}
	}
}

pub struct WindowSurface<'a> {
	pub api_instance: &'a APIInstance,
	// pub os_window: &'a winit::window::Window,
	pub window: &'a OSWindow,
	pub surface: vk::SurfaceKHR,
}

impl<'a> WindowSurface<'a> {
	pub fn new(api_instance: &'a APIInstance, window: &'a OSWindow) -> WindowSurface<'a> {

		let surface = unsafe { ash_window::create_surface(
			&api_instance.entry,
			&api_instance.instance,
			window.os_window.raw_display_handle(),
			window.os_window.raw_window_handle(),
			None,
			) }
			.unwrap();

		// WindowSurface { api_instance, os_window: &window.os_window, surface }
		WindowSurface { api_instance, window, surface }
	}
}
