use std::sync::{Arc, RwLock};

use winit::{window::WindowBuilder, dpi::LogicalSize, event_loop::EventLoop};


pub struct OSWindow {
	pub height: u32,
	pub width: u32,
	pub title: String,

	// TODO: higher level event loop
	pub os_event_loop: Arc<RwLock<EventLoop<()>>>,
	pub os_window: winit::window::Window,
}
impl OSWindow {

	// creates window and event loop
	pub fn new(width: u32, height: u32, title: &String
			   , event_loop: &Arc<RwLock<EventLoop<()>>>) -> OSWindow {

		let event_loop_handle = &event_loop.read().unwrap();
		let window = WindowBuilder::new()
			.with_title("Vulkan Triangle Test")
			.with_inner_size(LogicalSize::new(width, height))
			.build(&event_loop_handle)
			.unwrap();

		OSWindow { width, height, title: title.clone(),  
			os_event_loop: Arc::clone(&event_loop), os_window: window,
		}
	}
}

