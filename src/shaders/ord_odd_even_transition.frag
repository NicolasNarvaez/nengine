#version 450

layout(location = 0) in vec2 glTexCoord;  
layout(location = 0) out vec4 outColor;

layout (binding = 0) uniform UniformParams {
	// vec2 glTexCoord;
	vec3 Param1;
	vec3 Param2;
} UP;
layout (binding = 1) uniform sampler2D Data;	

#define OwnPos glTexCoord	 // contents of the uniform data fields  
#define TwoStage UP.Param1.x	 // number of stages ? (tx width ??)
#define Pass_mod_Stage UP.Param1.y	// left boundary for current pass? 
#define TwoStage_PmS_1 UP.Param1.z	// right boundary for current pass?
#define Width UP.Param2.x	// 2D texture width
#define Height UP.Param2.y	// 2D texture height
#define Pass UP.Param2.z  // parallel compare ops index (sorting network) ????
				// seems to be the neighbour traslation (width)
void main(void)  {	  // get self	   
	// vec4 self = texture2D(Data, OwnPos.xy);
	vec4 self = texture(Data, OwnPos.xy);	 
	float i = floor(OwnPos.x * Width) + floor(OwnPos.y * Height) * Width;	   // my position within the range to merge    
	float j = floor(mod(i, TwoStage));		
	float compare;		

	if ( (j < Pass_mod_Stage) || (j > TwoStage_PmS_1) )		 
		// must copy -> compare with self		 
		compare = 0.0;		
	else		
		// must sort		
		if ( mod((j + Pass_mod_Stage) / Pass, 2.0) < 1.0)		   
			// we are on the left side -> compare with partner on the right			   
			compare = 1.0;		  
		else		  
			// we are on the right side -> compare with partner on the left			 
			compare = -1.0;		   
	// get the partner		
	float adr = i + compare * Pass;		 
	vec4 partner = texture(Data, vec2(floor(mod(adr, Width)) / Width,											 
									floor(adr / Width) / Height));		  
	// on the left it's a < operation; on the right it's a >= operation		   
	outColor = (self.x * compare < partner.x * compare) ? self : partner;  
}

