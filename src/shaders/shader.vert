# version 450

//glslc shader.vert -o vert.spv

// Model coords
//vec2 positions[3] = vec2[](
		//vec3(0.0, -0.5),
		//vec3(0.5, 0.5),
		//vec3(-0.5, 0.5)
//);

// NDC  coords
vec2 positions[3] = vec2[](
		vec2(0.0, -0.5),
		vec2(0.5, 0.5),
		vec2(-0.5, 0.5)
);

vec3 colors[3] = vec3[](
		vec3(1.0, 0.0, 0.0),
		vec3(0.0, 1.0, 0.0),
		vec3(0.0, 0.0, 1.0)
);

layout(binding = 0) uniform UniformBufferObject {
	mat4 model;
	mat4 view;
	mat4 proj;
} ubo;
layout(set = 0, binding = 2) uniform EntityData {
	vec4 pos;
	mat4 rot;
} entity_data;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexCoord;

mat4 ubo_model = mat4(1.0);
mat4 ubo_view = mat4(1.0);
mat4 ubo_projection = mat4(1.0);

void main() {
		//gl_Position = vec4(positions[gl_VertexIndex % 3], 0.0, 1.0);
		fragColor = colors[gl_VertexIndex % 3];
		//gl_Position = ubo.proj * ubo_view * ((ubo_model * vec4(positions[gl_VertexIndex % 3], 0.0, 1.0)) + vec4(0.0, 0.0, 0.0, 0.0));
		// gl_Position = ubo.proj * ubo.view * ((ubo.model * vec4(positions[gl_VertexIndex % 3], 0.0, 1.0)) + entity_data.pos);

		gl_Position = ubo.proj * ubo.view * ((ubo.model * vec4(inPosition, 1.0)) + entity_data.pos);
		//fragColor = inColor;
		fragTexCoord = inTexCoord;
}

