## WORK IN PROGRESS

This is a personal work, with self-referencing notes, beware of code-delirium.

Im rewritting an old [n-d web simulation engine](https://github.com/NicolasNarvaez/NEngine.js) into modern soil (?) c++, just for the sake 
of it (?).

First i will not be searching speed, just to make an engine core flexible enough
for the "primary task", the fns written will vary in incompleteness and 
whishfullness. An FP approach and "ontology" based solutions, should keep it 
modular enough to add optimizations later (and rewritte a ton of this).

The "primary task":
nd engine: graphics/physics 
graphics: correct n-d rendering (volumetric rendering && superposition), vol-RTX
physics: base solids, constraints

The "primary requirements":
multi-platform nativeness, gpu agnostic, but rtx derived preferred.
wasm first and rtx first (the closest way always)
ND first (optimizations could reach the hot-reload, its ok), including shadding

There will be no more "tasks" or "issues" whatsoever, until the primary work is 
done. But the immediate desired requirement affter it is integrating curve and 
non-continous space types (the so desired "SpaceGraph").

(I recognize there is much to learn, for the rest of life, so please bear with 
my ignorance.)

There is a C++ "shadow" of the rust codebase along it, its to test algorithms on both languages
easily, it should be put on its own repository after stabilization.
