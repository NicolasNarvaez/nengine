use std::{path::{PathBuf, Path}, env, fs};


fn get_output_path() -> PathBuf {
    //<root or manifest path>/target/<profile>/
    let manifest_dir_string = env::var("CARGO_MANIFEST_DIR").unwrap();
    let build_type = env::var("PROFILE").unwrap();
    let path = Path::new(&manifest_dir_string).join("target").join(build_type);
    return PathBuf::from(path);
}

// and then inside my build.rs main() method:
fn main() {
    println!("writing files: ");
    let out_dir = env::var("OUT_DIR").unwrap();
    let target_dir = get_output_path();
    let current_dir = env::current_dir().unwrap();
    println!("out_dir: {:?}", out_dir);
    println!("target_dir: {:?}", target_dir);
    println!("current_dir: {:?}", current_dir);

    let dest_path = Path::join(&current_dir, "resources/shaders");
    fs::create_dir_all(dest_path).unwrap();

    let src = Path::join(&current_dir, "src/shaders/frag.spv");
    let dest = Path::join(&current_dir, "resources/shaders/frag.spv");
    println!("src: {:?}", src);
    println!("dest: {:?}", dest);
    fs::copy(src, dest).unwrap();

    let src_2 = Path::join(&current_dir, "src/shaders/vert.spv");
    let dest_2 = Path::join(&current_dir, "resources/shaders/vert.spv");
    fs::copy(src_2, dest_2).unwrap();

    let src_2 = Path::join(&current_dir, "src/shaders/ord_odd_even_transition.spv");
    let dest_2 = Path::join(&current_dir, "resources/shaders/ord_odd_even_transition.spv");
    fs::copy(src_2, dest_2).unwrap();

// ...
}
